extern void __VERIFIER_error() __attribute__ ((__noreturn__));
extern void __VERIFIER_assume(int);
extern int __VERIFIER_nondet_int();

/*
 
 We want something where hitting the
 error depends on the values of two
 fresh reads within a loop. As of
 4/12/17, the SV-COMP transformer is
 incorrect with respect to loops and
 how it rewrites symbolic reads.

*/

int main( ) {

  int x = 0;

  for (int i=0; i<2; i++) {
    x = __VERIFIER_nondet_int();
    if (x >= 0) {
      return 0;
    }
  } 

  /* PC = (X0 < 0) && (X1 < 0) */
  __VERIFIER_error();

}

