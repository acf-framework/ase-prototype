require 'colorize'
CFC = "ruby /home/mgerrard/work/cfc/src/cfc.rb"

def check ( test_name, *analysis )

  if (analysis.any? && analysis.first == "CPA")
    `#{CFC} --ua_timeout 1 #{test_name}.c`
  elsif (analysis.any? && analysis.first == "UA")
    `#{CFC} --cpa_timeout 1 #{test_name}.c`
  elsif (analysis.any? && analysis.first == "single_cpa")
    `#{CFC} --single_cpa #{test_name}.c`
  else
    `#{CFC} #{test_name}.c`
  end
  

  oracle = "./#{test_name}.oracle"
  result = "./analysis_logs/#{test_name}/LOG"
  # Ignore any differences in analysis time
  difference = `diff -I '.*time:.*' #{oracle} #{result}`

  if difference.empty?
    puts "#{test_name}: PASS".green
  else
    puts "#{test_name}: FAIL".red
    puts "** Difference:".yellow
    puts difference.yellow
  end
  
end

### RUN TESTS ###

all_tests = [
  ["one_blocking_clause"],
  ["two_blocking_clauses"],
  ["always_hits_error"],
  ["multiple_evaluations_in_guard", "CPA"],
  ["identical_guard_conditions", "CPA"],
  ["loop_head_guards", "CPA"],
  ["loops"],
  ["motivating_example"],
  ["nonlinear_motivating_example", "single_cpa"]
]

if (ARGV.size == 0)
  puts "Running *all* tests.".yellow
  all_tests.each do |test|
    check(*test)
  end
elsif (ARGV.size == 1)
  specified_test = ARGV[0]
  puts "You only want to run: #{specified_test}".yellow
  test = all_tests.find {|t| t.first == specified_test}
  if test
    check(*test)
  else
    puts "There is no test named #{specified_test}. Aborting".red
    abort
  end
else
  puts '  Usage: ruby run_tests.rb [file_name_without_extension]'.red
  abort
end

