extern void __VERIFIER_error() __attribute__ ((__noreturn__));
extern void __VERIFIER_assume(int);
extern int __VERIFIER_nondet_int();

/*  
 The branch directives should be: [0, 1]
*/

int main( ) {

  int x = __VERIFIER_nondet_int();
  int y = 42;

  if (x == 1 && x == 1) {
    y = -1;
  } 

  if (y > 0) {
    __VERIFIER_error();
  }

}

