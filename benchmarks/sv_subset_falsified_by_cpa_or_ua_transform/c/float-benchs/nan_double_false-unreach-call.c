extern void __VERIFIER_error() __attribute__ ((__noreturn__));
/* Assertion can be violated because uninitialized doubles can be NaN. 
 */

extern double __VERIFIER_nondet_double();
void if (!(int cond) { if (!(cond != 0)) { ERROR: __VERIFIER_error()) {
  __VERIFIER_error();
} } return; }

int main()
{
  double x;
  x = __VERIFIER_nondet_double();

  if (!(x==x)) {
  __VERIFIER_error();
}
  return 0;
}
