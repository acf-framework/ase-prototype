require 'nokogiri'

def reports_error(n)
  return (n.css("column[title='status']").first['value'] == "false(unreach-call)")
end

def error_exists(n)
  return (n.css("column[title='category']").first['value'] == "correct")
end

def found_error(n)
  return (reports_error(n) and error_exists(n))
end

def get_program_name(n)
  relative_path = n["name"]
  sv_path = relative_path.split('/')[1..-1].join('/')
  return sv_path
end

def get_cpu_time(n)
  e = n.css("column[title=cputime]")
  time = e.first['value'][0..-2] # strip 's' at the end
  return time.to_i
end

def get_tool_name(doc)
  return doc.css("result").first["benchmarkname"]
end

class SvResult
  attr_reader :program, :tool_name, :cpu_time
  def initialize ( p, t, c )
    @program = p; @tool_name = t; @cpu_time = c
  end
end

cpa_results_dir = "../CPA"
ua_results_dir = "../UA"

falsified_results = []

tool_dirs = [cpa_results_dir, ua_results_dir]
tool_dirs.each do |tool_dir|
  Dir.foreach(tool_dir) do |category_xml|
    next if category_xml.start_with? '.'
    path = tool_dir+'/'+category_xml
    doc = File.open(path) { |f| Nokogiri::XML(f) }
    tool_name = get_tool_name(doc)

    result_nodes = doc.css("run")
    result_nodes.each do |r|
      if found_error(r)
        program = get_program_name(r)
        cpu_time = get_cpu_time(r)
        falsified_results << SvResult.new(program,tool_name,cpu_time)
      end
    end
  end
end

falsified_results.sort! {|a,b| a.program <=> b.program}

falsified_programs = falsified_results.map {|e| e.program}
falsified_programs.uniq!

class ProgramConfig
  attr_reader :program, :fastest_tool, :time
  def initialize ( p, f, t )
    @program=p; @fastest_tool=f; @time=t
  end
end

programs = []
falsified_programs.each do |program|
  rs = falsified_results.select {|r| r.program == program}
  cpu_times = rs.map {|r| r.cpu_time}
  max_cpu_time = cpu_times.max
  fastest_tool = rs.min_by(&:cpu_time).tool_name
  programs << ProgramConfig.new(program, fastest_tool, max_cpu_time)
end

programs.each_with_index do |p, i|
  file_path = '../'+p.program.gsub('sv-benchmarks', 'sv_subset_falsified_by_cpa_or_ua_transform')
  make_script_cmd = "ruby make_single_overapprox_script.rb "\
                    "#{file_path} #{p.fastest_tool} #{p.time}"
  `#{make_script_cmd}`
end
