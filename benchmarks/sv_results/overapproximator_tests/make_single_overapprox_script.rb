def get_configurations ( first_tool )

    configurations = []

    case first_tool
    when "uautomizer"
      configurations = [
        ["UltimateAutomizer"],
        ["CPA", "sv-comp17"],
        ["CPA", "sv-comp16-bam"],
        ["CPA", "sv-comp17-k-induction"]
      ]
    when "cpa-seq"
      configurations = [
        ["CPA", "sv-comp17"],
        ["CPA", "sv-comp16-bam"],
        ["CPA", "sv-comp17-k-induction"],
        ["UltimateAutomizer"]
      ]
    when "cpa-bam-bnb"
      configurations = [
        ["CPA", "sv-comp16-bam"],
        ["CPA", "sv-comp17"],
        ["CPA", "sv-comp17-k-induction"],
        ["UltimateAutomizer"]
      ]
    when "cpa-kind"
      configurations = [
        ["CPA", "sv-comp17-k-induction"],
        ["CPA", "sv-comp17"],
        ["CPA", "sv-comp16-bam"],
        ["UltimateAutomizer"]
      ]
    end

    return configurations

end

UA_DIR = File.expand_path('../../../analyzers/uautomizer', File.dirname(__FILE__))
CPA_DIR = File.expand_path('../../../analyzers/cpachecker', File.dirname(__FILE__))

def make_ua_cmd ( program, time )
  ua_script = "#{UA_DIR}/Ultimate.py"
  ua_specification = "#{UA_DIR}/PropertyUnreachCall.prp"
  ua_cmd = "#{ua_script} "\
           "--full-output "\
           "--spec #{ua_specification} "\
           "--architecture 32bit "\
           "--file #{program}"

  return "timeout #{time} #{ua_cmd}"
end

def make_cpa_cmd ( program, config, time )
  cpa_script = "#{CPA_DIR}/scripts/cpa.sh"
  cpa_specification = "#{CPA_DIR}/config/specification/sv-comp-reachability.spc"
  
  # 900s is required using any of the 2017 configs
  cpa_cmd = "#{cpa_script} -#{config} "\
            "-timelimit 900 "\
            "-spec #{cpa_specification} "\
            "#{program}"
  
  return "timeout #{time} #{cpa_cmd}"
end

def make_cmd ( tool, program, time )
  padding = 60 # seconds
  timeout = time.to_i + padding
  program = '../'+program.gsub("sv-benchmarks", "sv_subset_falsified_by_cpa_or_ua_transform")
  
  if tool.first == "CPA"
    config = tool[1]
    return make_cpa_cmd(program, config, timeout)
  elsif tool.first == "UltimateAutomizer"
    return make_ua_cmd(program, timeout)
  else
    puts "Not expecting: #{tool.first}".red; abort
  end
end

unless ARGV.size == 3
  puts "Expecting three arguments: program, fastest_tool, time"
end

def make_script_string(program, cmds, configs)

return <<-SCRIPT
require 'open3'

found_error = false
error_finding_config = ""

configs = #{configs}

commands = #{cmds}
commands.each_with_index do |cmd, i|
  puts "Trying to run: "+cmd
  stdout, stderr, status = Open3.capture3(cmd)

  if stdout.include? "FALSE"
    found_error = true
    error_finding_config = configs[i].last
  end
  
  break if found_error
end
  
if found_error
  puts "\n  SUCCESS with "+error_finding_config+": #{program}"
else
  puts "\n  FAIL: #{program}"
end

SCRIPT
end

program = ARGV[0]
fastest_tool = ARGV[1]
time = ARGV[2]

configs = get_configurations(fastest_tool)
cmds = configs.map {|c| make_cmd(c, program, time)}

script_string = make_script_string(program, cmds, configs)
dir_and_program_name = program.split('/c/').last.split('/').join('__')
script_path = "scripts/single-overs__#{dir_and_program_name}.rb"
File.write(script_path, script_string)
