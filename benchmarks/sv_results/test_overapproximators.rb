require 'nokogiri'

def reports_error(n)
  return (n.css("column[title='status']").first['value'] == "false(unreach-call)")
end

def error_exists(n)
  return (n.css("column[title='category']").first['value'] == "correct")
end

def found_error(n)
  return (reports_error(n) and error_exists(n))
end

def get_program_name(n)
  relative_path = n["name"]
  sv_path = relative_path.split('/')[1..-1].join('/')
  return sv_path
end

def get_cpu_time(n)
  e = n.css("column[title=cputime]")
  time = e.first['value'][0..-2] # strip 's' at the end
  return time.to_i
end

def get_tool_name(doc)
  return doc.css("result").first["benchmarkname"]
end

def get_configurations ( first_tool )

    configurations = []

    case first_tool
    when "uautomizer"
      configurations = [
        ["UltimateAutomizer"],
        ["CPA", "sv-comp17"],
        ["CPA", "sv-comp16-bam"],
        ["CPA", "sv-comp17-k-induction"]
      ]
    when "cpa-seq"
      configurations = [
        ["CPA", "sv-comp17"],
        ["CPA", "sv-comp16-bam"],
        ["CPA", "sv-comp17-k-induction"],
        ["UltimateAutomizer"]
      ]
    when "cpa-bam-bnb"
      configurations = [
        ["CPA", "sv-comp16-bam"],
        ["CPA", "sv-comp17"],
        ["CPA", "sv-comp17-k-induction"],
        ["UltimateAutomizer"]
      ]
    when "cpa-kind"
      configurations = [
        ["CPA", "sv-comp17-k-induction"],
        ["CPA", "sv-comp17"],
        ["CPA", "sv-comp16-bam"],
        ["UltimateAutomizer"]
      ]
    end

    return configurations

end

class SvResult
  attr_reader :program, :tool_name, :cpu_time
  def initialize ( p, t, c )
    @program = p; @tool_name = t; @cpu_time = c
  end
end

cpa_results_dir = "./CPA"
ua_results_dir = "./UA"

falsified_results = []

tool_dirs = [cpa_results_dir, ua_results_dir]
tool_dirs.each do |tool_dir|
  Dir.foreach(tool_dir) do |category_xml|
    next if category_xml.start_with? '.'
    path = tool_dir+'/'+category_xml
    doc = File.open(path) { |f| Nokogiri::XML(f) }
    tool_name = get_tool_name(doc)

    result_nodes = doc.css("run")
    result_nodes.each do |r|
      if found_error(r)
        program = get_program_name(r)
        cpu_time = get_cpu_time(r)
        falsified_results << SvResult.new(program,tool_name,cpu_time)
      end
    end
  end
end

falsified_results.sort! {|a,b| a.program <=> b.program}

falsified_programs = falsified_results.map {|e| e.program}
falsified_programs.uniq!

class ProgramConfig
  attr_reader :program, :fastest_tool, :time
  def initialize ( p, f, t )
    @program=p; @fastest_tool=f; @time=t
  end
end

programs = []
falsified_programs.each do |program|
  rs = falsified_results.select {|r| r.program == program}
  cpu_times = rs.map {|r| r.cpu_time}
  max_cpu_time = cpu_times.max
  fastest_tool = rs.min_by(&:cpu_time).tool_name
  programs << ProgramConfig.new(program, fastest_tool, max_cpu_time)
end

require 'colorize'

UA_DIR = File.expand_path('../../analyzers/uautomizer', File.dirname(__FILE__))
CPA_DIR = File.expand_path('../../analyzers/cpachecker', File.dirname(__FILE__))

def make_ua_cmd ( program, time )
  ua_script = "#{UA_DIR}/Ultimate.py"
  ua_specification = "#{UA_DIR}/PropertyUnreachCall.prp"
  ua_cmd = "#{ua_script} "\
           "--full-output "\
           "--spec #{ua_specification} "\
           "--architecture 32bit "\
           "--file #{program}"

  return "timeout #{time} #{ua_cmd}"
end

def make_cpa_cmd ( program, config, time )
  cpa_script = "#{CPA_DIR}/scripts/cpa.sh"
  cpa_specification = "#{CPA_DIR}/config/specification/sv-comp-reachability.spc"
  
  # 900s is required using any of the 2017 configs
  cpa_cmd = "#{cpa_script} -#{config} "\
            "-timelimit 900 "\
            "-spec #{cpa_specification} "\
            "#{program}"
  
  return "timeout #{time} #{cpa_cmd}"
end

def make_cmd ( tool, program, time )
  padding = 60 # seconds
  timeout = time + padding
  program = program.gsub "sv-benchmarks", "sv_subset_falsified_by_cpa_or_ua_transform"
  
  if tool.first == "CPA"
    config = tool[1]
    return make_cpa_cmd(program, config, timeout)
  elsif tool.first == "UltimateAutomizer"
    return make_ua_cmd(program, timeout)
  else
    puts "Not expecting: #{tool.first}".red; abort
  end
end

require 'open3'

programs.each_with_index do |p, iter|
  next unless iter > 11
  puts "\n** Iteration: #{iter} **\n\n"
#  next unless p.program == "../sv-benchmarks/c/ddv-machzwd/ddv_machzwd_all_false-unreach-call_true-valid-memsafety.i"  

  configs = get_configurations(p.fastest_tool)
  found_error = false
  error_finding_config = ""

  configs.each do |c|
    cmd = make_cmd(c, p.program, p.time)
    puts "Trying to run: #{cmd}"
    stdout, stderr, status = Open3.capture3(cmd)

    if stdout.include? "FALSE"
      found_error = true
      error_finding_config = c
    end
  
    break if found_error
  end
  
  if found_error
    puts "\n\n  Configuration #{error_finding_config} found an error in #{p.program}"
  else
    puts "\n\n  FAIL: No configurations found errors in #{p.program}."
  end
 
end
