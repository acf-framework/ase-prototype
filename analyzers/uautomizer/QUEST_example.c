extern void __VERIFIER_error() __attribute__ ((__noreturn__));
extern void __VERIFIER_assume(int);
extern int __VERIFIER_nondet_int();

int main( ) {
  int x = __VERIFIER_nondet_int();
  int y = __VERIFIER_nondet_int();

  __VERIFIER_assume(x>=0); 
  __VERIFIER_assume(y>=0);

  int tmp = 0;

  if (x < y) {
    //    tmp = x*x + y*y;
    tmp = -1;
  } else {
    tmp = x*x + y*y;
    //    tmp = -1;
  }

  if (!(tmp > 0)) {
    __VERIFIER_assume(!(x < y));
    __VERIFIER_error();
  }
}
