keep_no_input_read_programs = false
if ARGV.size == 1
  if ARGV[0] == "--keep_no_input"
    keep_no_input_read_programs = true
  else
    puts "Invalid argument: #{ARGV[0]}. Aborting."
    abort
  end
end

analyses_dir = "#{Dir.pwd}/analysis_logs/"
statistics_dir = "#{Dir.pwd}/statistics"

tools_dir = File.dirname(__FILE__)

class Statistic

  attr_reader :analysis_time
  attr_reader :possible_failure_time
  attr_reader :generalization_time
  attr_reader :definite_failure_time
  attr_reader :slice_time

  attr_reader :ua_timeouts
  attr_reader :cpa_timeouts
  attr_reader :civl_timeouts

  attr_reader :iterations
  attr_reader :generalizations
  attr_reader :spurious_errors

  attr_reader :conjuncts_sliced
  attr_reader :total_partitions
  attr_reader :partitions_without_gap

  def initialize ( 
      a_time, poss_time, gen_time, def_time, slice_time, 
      ua_to, cpa_to, civl_to, 
      iters, gens, spurs,
      c_sliced, part_gap, part_no_gap
    )

    @analysis_time = a_time
    @possible_failure_time = poss_time
    @generalization_time = gen_time
    @definite_failure_time = def_time
    @slice_time = slice_time

    @ua_timeouts = ua_to
    @cpa_timeouts = cpa_to
    @civl_timeouts = civl_to

    @iterations = iters
    @generalizations = gens
    @spurious_errors = spurs
 
    @conjuncts_sliced = c_sliced
    @total_partitions = part_gap
    @partitions_without_gap = part_no_gap

  end

end

def get_stat ( keyword, file_str )
  regex = "#{keyword}: "+'(\d+)'
  return file_str.scan(/#{regex}/).first.first
end

stats = []

stat_strings = [
  "Analysis time",
  "Possible failure time",
  "Generalization time",
  "Definite failure time",
  "Slice time",

  "UA timeouts",
  "CPA timeouts",
  "CIVL timeouts",

  "Iterations",
  "Generalizations",
  "Spurious errors",

  "Conjuncts sliced",
  "Total partitions",
  "Partitions without gap"
]
stat_keys = stat_strings.map {|k| k.downcase.gsub(' ','_')}

Dir.foreach(analyses_dir) do |analysis|
  log_file = analyses_dir+analysis+'/LOG'
  next unless File.exist? log_file
  log = File.read(log_file)
  unless keep_no_input_read_programs
    next if log =~ /All executions lead to an error/
  end
  next unless log =~ /CFC RESULTS/ # finished analysis

  stats_to_splat = []
  stat_strings.each do |s|
    stats_to_splat << get_stat(s, log)
  end

  stats << Statistic.new(*stats_to_splat)
end

Dir.mkdir statistics_dir unless File.exist? statistics_dir

stat_keys.each do |stat|
  stat_file = statistics_dir + "/#{stat}.dat"
  File.open(stat_file, "w+") do |f|
    stats.each {|s| f.puts eval("s.#{stat}")}
  end
end

############################################################
# Call single impulse plotting for each stat
single_impulse_script = "#{tools_dir}/plotsorted.sh"

stat_keys.each do |stat|
  stat_file = statistics_dir + "/#{stat}.dat"
  `#{single_impulse_script} #{stat_file} #{stat}`
end

############################################################
# Call stacked impulse plotting for disjoint partition stats
stacked_partition_script = "#{tools_dir}/plotdisjointparitions.sh"
total_parts_file = statistics_dir + "/total_parts.dat"
equiv_parts_file = statistics_dir + "/equiv_parts.dat"

class PartitionImpulse 
  attr_reader :total_parts
  attr_reader :equiv_parts

  def initialize ( tp, ep )
    @total_parts = tp
    @equiv_parts = ep
  end
end

partition_impulses = []
stats.each do |st|
  partition_impulses << PartitionImpulse.new(st.total_partitions.to_i, st.partitions_without_gap.to_i)
end
partition_impulses.sort! {|a,b| a.total_parts <=> b.total_parts}
partition_impulses.reverse!

File.open(total_parts_file, "w+") do |f|
  partition_impulses.each {|p| f.puts p.total_parts}
end

File.open(equiv_parts_file, "w+") do |f|
  partition_impulses.each {|p| f.puts p.equiv_parts}
end

`#{stacked_partition_script} #{total_parts_file} #{equiv_parts_file}`

###########################################################
# Call stacked impulse plotting for analysis times
stacked_times_script = "#{tools_dir}/plottimes.sh"

class TimeImpulse
  attr_reader :gen_poss_def_slice
  attr_reader :gen_poss_def
  attr_reader :gen_poss
  attr_reader :gen

  def initialize ( g, p, d, s )
    @gen_poss_def_slice = g+p+d+s
    @gen_poss_def = g+p+d
    @gen_poss = g+p
    @gen = g
  end
end

time_impulses = []
stats.each do |st|
  g = st.generalization_time.to_i
  p = st.possible_failure_time.to_i
  d = st.definite_failure_time.to_i
  s = st.slice_time.to_i

  time_impulses << TimeImpulse.new(g,p,d,s)
end
time_impulses.sort! {|a,b| a.gen_poss_def_slice <=> b.gen_poss_def_slice}
time_impulses.reverse!

gen_file = statistics_dir + "/gen.dat"
File.open(gen_file, "w+") do |f|
  time_impulses.each {|t| f.puts t.gen}
end

gen_poss_file = statistics_dir + "/gen_poss.dat"
File.open(gen_poss_file, "w+") do |f|
  time_impulses.each {|t| f.puts t.gen_poss}
end

gen_poss_def_file = statistics_dir + "/gen_poss_def.dat"
File.open(gen_poss_def_file, "w+") do |f|
  time_impulses.each {|t| f.puts t.gen_poss_def}
end

gen_poss_def_slice_file = statistics_dir + "/gen_poss_def_slice.dat"
File.open(gen_poss_def_slice_file, "w+") do |f|
  time_impulses.each {|t| f.puts t.gen_poss_def_slice}
end

`#{stacked_times_script} #{gen_poss_def_slice_file} #{gen_poss_def_file} #{gen_poss_file} #{gen_file}`

#########################################################
# Put those picture where they belong

`mv *.pdf #{statistics_dir}`
