    require 'colorize'

    require 'optparse'

    require 'fileutils'

    require 'open3'

    require 'nokogiri'

    require 'pp'

    CIVL_DIR = File.expand_path('../analyzers', File.dirname(__FILE__))
    UA_DIR = File.expand_path('../analyzers/uautomizer', File.dirname(__FILE__))
    CPA_DIR = File.expand_path('../analyzers/cpachecker', File.dirname(__FILE__))

    unknown = 0
    safe = 1
    error_found = 2
    cached = 3
    iteration = 0

    # UA_DIR defined in ./system_dependencies.rb
    ua_script = "#{UA_DIR}/Ultimate.py"
    ua_specification = "#{UA_DIR}/PropertyUnreachCall.prp"
    ua_timeouts = 0
    TIMEOUT_STATUS = 124 # the exit status when `timeout` times out

    # CPA_DIR defined in ./system_dependencies.rb 
    cpa_script = "#{CPA_DIR}/scripts/cpa.sh"
    cpa_specification = "#{CPA_DIR}/config/specification/sv-comp-reachability.spc"
    cpa_debug = true

    cpa_timeouts = 0

    guidance_file = ""

    # CIVL_DIR defined in ./system_dependencies.rb
    civl_jar = "java -jar -ea #{CIVL_DIR}/civl.jar"
    civl_debug = true
    civl_timeouts = 0

    spurious_error_count = 0

    trace_id = 0
    svcomp_error_summary = ""

    input_counts = {}
    input_types = {}
    symvars_to_lines = {}
    $lines_to_vars = {}
    $raw_to_pretty_reads = {}

    spurious_errors = []

    conjuncts_sliced = 0

    always_fails = false

    top_of_lattice = ["1"]   # logical 'true' in C
    generalization_count = 0
    analysis_moved_to_top = false
    just_generalized = false

  #  Input  : upper_C, stalling_c
  #  Output : generalized_blocking_clause

  #  1. max_{c \in upper_C} #(c \cap stalling_c)
  #  2. base = c \cap upper_C
  #  3. foreach x \in {stalling_c - base}:
  #       does over_C \cup {stalling_c - x}
  #       successfully block?

  #  Data structure implementation:
  #    upper_C     : an array of array of strings
  #    stalling_c : an array of strings 

    class Stopwatch

    attr_reader :time_spent

    def initialize
      @time_spent = 0
      @clock_is_running = false
    end
    
    def start
      assert { not @clock_is_running }
      @clock_is_running = true
      @start_time = Time.now
    end

    def stop
      assert { @clock_is_running }
      @clock_is_running = false
      @end_time = Time.now
      @time_spent += (@start_time - @end_time).to_i.abs    
    end

  end


    class Branch
      attr_accessor :conditional, :branch_taken, :line_number, :is_loop_head

      def initialize( branch, loop_head_flag )
        @conditional = extract_conditional(branch)
        @branch_taken = discover_branch_taken(branch)
        @line_number = extract_line_number(branch)
        @is_loop_head = loop_head_flag
      end

      def extract_line_number( branch )
        line_number = branch.css("data[key='startline']").text
        return line_number.to_i
      end

      def discover_branch_taken( branch )
        if branch.css("data[key='control']").text == "condition-true"
          return 1
        else
          return 0
        end
      end

      def extract_conditional( branch )
        raw_cond = select_conditional_source(branch)
        conditional = ''
        if conditional[1] == '!'
          conditional = raw_cond[3..-3]
        else 
          conditional = raw_cond[1..-2] # Strip enclosing braces
        end
        return conditional
      end

      def select_conditional_source( branch )
        return branch.css("data[key='sourcecode']").text
      end
    end

    class Guidance
      attr_accessor :program_name, :branch_lines, :outcomes 
    end

    class Instrumentation
      attr_accessor :program, :assumptions, :symvars_to_lines, :input_counts, :input_types

      def initialize (program, assumptions, sym_map, count_map, type_map)
        @program = program
        @assumptions = assumptions
        @symvars_to_lines = sym_map
        @input_counts = count_map
        @input_types = type_map
      end

      def display
        puts "Global instrumentation object:"; puts
        puts "  Program: #{@program}"
        puts "  Assumptions: #{@assumptions}"
      end
    end

      class Stopwatch

      attr_reader :time_spent

      def initialize
        @time_spent = 0
        @clock_is_running = false
      end
      
      def start
        assert { not @clock_is_running }
        @clock_is_running = true
        @start_time = Time.now
      end

      def stop
        assert { @clock_is_running }
        @clock_is_running = false
        @end_time = Time.now
        @time_spent += (@start_time - @end_time).to_i.abs    
      end

    end


      class Branch
        attr_accessor :conditional, :branch_taken, :line_number, :is_loop_head

        def initialize( branch, loop_head_flag )
          @conditional = extract_conditional(branch)
          @branch_taken = discover_branch_taken(branch)
          @line_number = extract_line_number(branch)
          @is_loop_head = loop_head_flag
        end

        def extract_line_number( branch )
          line_number = branch.css("data[key='startline']").text
          return line_number.to_i
        end

        def discover_branch_taken( branch )
          if branch.css("data[key='control']").text == "condition-true"
            return 1
          else
            return 0
          end
        end

        def extract_conditional( branch )
          raw_cond = select_conditional_source(branch)
          conditional = ''
          if conditional[1] == '!'
            conditional = raw_cond[3..-3]
          else 
            conditional = raw_cond[1..-2] # Strip enclosing braces
          end
          return conditional
        end

        def select_conditional_source( branch )
          return branch.css("data[key='sourcecode']").text
        end
      end

      class Guidance
        attr_accessor :program_name, :branch_lines, :outcomes 
      end

      class Instrumentation
        attr_accessor :program, :assumptions, :symvars_to_lines, :input_counts, :input_types

        def initialize (program, assumptions, sym_map, count_map, type_map)
          @program = program
          @assumptions = assumptions
          @symvars_to_lines = sym_map
          @input_counts = count_map
          @input_types = type_map
        end

        def display
          puts "Global instrumentation object:"; puts
          puts "  Program: #{@program}"
          puts "  Assumptions: #{@assumptions}"
        end
      end


    class AssertionError < RuntimeError
    end

    def assert &block
      raise AssertionError unless yield
    end

    def get_bare_name ( program )
      # Drop the extension after the *final* period
      name_without_ext = program.split('.')[0..-2].join('.')
      bare_name = name_without_ext.split('/').last
      return bare_name
    end

    def prepend_error_at_line ( file, target_line )

      curr_line = 1
      lines = IO.readlines(file).map do |line|
        if curr_line == target_line
          line = '__VERIFIER_error(); //' + line
        end
        curr_line += 1
        line
      end
       
      File.open(file, 'w') do |f|
        f.puts lines
      end

    end

    def rewrite_line_with_string ( file, target_line, string )

      curr_line = 1
      lines = IO.readlines(file).map do |line|
        if curr_line == target_line
          line = string
        end
        curr_line += 1
        line
      end
       
      File.open(file, 'w') do |f|
        f.puts lines
      end

    end

    # The canonical string for an abstract witness will
    # be space delimited tuples of:
    #   (branch, branch_outcome)
    def get_canonical_string (witness)

      string = ""

      witness.branch_lines.each_with_index do |branch, i|
        outcome = witness.outcomes[i]
        string += "(#{branch},#{outcome}) "
      end

      return string

    end

    def get_abstract_witness (witness_graphml, is_cpa)

      graphml_file = File.read(witness_graphml)
      witness = Nokogiri::XML.parse(graphml_file)

      guidance = Guidance.new

      branches = get_branches_in_error_path(witness, is_cpa)
      guidance.branch_lines = branches.map { |b| b.line_number }
      guidance.outcomes = branches.map { |b| b.branch_taken }

      return guidance

    end

    def is_relevant_control_edge (edge)
      sourcecode = "data[key='sourcecode']"

      if edge.values.include? "sink"
        return false
      elsif edge.css(sourcecode).text.include? "instrumentation_index"
        return false
      elsif edge.css(sourcecode).text.include? "input_at_"
        return false
      elsif edge.css(sourcecode).text.include? "__VERIFIER_assume("
        return false
      elsif edge.css(sourcecode).text.include? "INPUTS_at_"
        return false
      elsif edge.css(sourcecode).text.include? "__CPAchecker_TMP"
        return false
      else
        return true
      end
    end

    def get_branches_in_error_path ( witness, is_cpa )

      branch_trace = []

      is_loop_head = false
      witness.css('edge').each do |edge|
        if edge.at("data[key='enterLoopHead']")
          is_loop_head = true
          next
        end
        if edge.at("data[key='control']")
          if is_relevant_control_edge(edge)
            branch_trace << Branch.new(edge, is_loop_head)
          end
        end
        is_loop_head = false
      end

      if is_cpa
          branches_to_remove = []
          
          branch_trace.each_cons(2) do |curr_b, next_b|
            if ((curr_b.line_number == next_b.line_number) &&
                (!curr_b.is_loop_head))
              branches_to_remove << curr_b
            end
          end  

          branch_trace = branch_trace - branches_to_remove

      end

      return branch_trace

    end

    def make_guidance_file ( guidance, dir, program_name )

      guidance_file = "#{dir}/directives"
      program_name = program_name.split('/')[-1]

      File.open(guidance_file, 'w') do |f|
        f.puts program_name
        f.puts "lines #{guidance.branch_lines.join(' ')}"
        f.puts "guide #{guidance.outcomes.join(' ')}"
      end

      return guidance_file

    end

    def result_string ( result )
      case result
      when 0 then "unknown"
      when 1 then "safe"
      when 2 then "error found"
      when 3 then "cached"
      else assert { puts 'Illegal result'.red; false }
      end
    end

    def svcomp_error_found ( civl_output )
      return civl_output.include? '$assert(0, "__VERIFIER_error() is called.\n")'
    end

    def timed_out ( civl_output )
      return civl_output.include? 'Time out.'
    end

    def no_relevant_error_found ( civl_output )
      if civl_output.include? 'The standard properties hold for all executions.'
        return true
      elsif civl_output.include? 'The program MAY NOT be correct.'
        # If we got here and 'VERIFIER_error' was not in the results, then either only picky CIVL errors were caught, the error bound was exceeded, or both.
        return true
      else
        return false
      end
    end

    def is_svcomp_error( violation_summary )
      return violation_summary.include? "__VERIFIER_error() is called."
    end

    def extract_trace_id ( violation_summary )
      trace_id_regex = 'Violation (\d) encountered at depth .*'
      return violation_summary[/#{trace_id_regex}/,1]
    end

    def update_symvars_to_lines ( curr_map, new_map )
      new_map.each do |line, var|
        curr_map[line] = var
      end
      return curr_map
    end

    def update_input_counts ( curr_map, new_map )
      instr_line = "1"
      new_map.each do |line, count|
        next if line == instr_line
        if curr_map.keys.include? line
          curr_map[line] += count
        else
          curr_map[line] = count
        end
      end
      return curr_map
    end

    def update_input_types ( curr_map, new_map )
      instr_line = "1"
      new_map.each do |line, type|
        next if line == instr_line
        curr_map[line] = type
      end
      return curr_map
    end

    def update_lines_to_vars ( curr_map, new_map )
      new_map.each do |line, var|
        curr_map[line] = var
      end
      return curr_map
    end

    def grab_section ( name, output )
      start = "BEGIN #{name}\n"
      finish = "END #{name}\n"
      return output[/#{start}(.*?)#{finish}/m, 1]
    end

    def parse_read_calls ( output )

      map_string = grab_section("SymVar -> Line -> Var", output)
      symvars_to_line = []

      map_string.each_line do |l|

        next if l == "\n"

        triple = l.strip.split(' ',3)
        line = triple[1]; lhs = triple[2]
     
        instrumentation_line = "1"
        next if line == instrumentation_line

        symvars_to_line << triple[0..1] 
        
      end

      symvars_to_line = Hash[symvars_to_line]
      return symvars_to_line

    end

    def get_number_sliced_away ( output )
      return grab_section("Number of Conjuncts Sliced Away", output).to_i
    end

    def make_line_to_var_map ( output )

      map_string = grab_section("SymVar -> Line -> Var", output)
      lines_to_vars = []
      map_string.each_line do |l|
        next if l == "\n"

        triple = l.strip.split(' ',3)
        lhs = triple[2]
        unless lhs =~ /.*INPUTS_at_(\d+).*/
          lines_to_vars << triple[1..2] 
        end
      end
      lines_to_vars = Hash[lines_to_vars]

      return lines_to_vars

    end

    def parse_instrument_lines ( output )
      lines_string = grab_section("Guidance Lines", output)
      lines = []
      lines_string.each_line {|l| next if l == "\n"; lines << l.strip}
      puts "Guidance Lines: #{lines}" if $debug
      return lines
    end

    def parse_input_frequency ( output )

      inputs_string = grab_section("Line -> Number of Input Reads", output)
      inputs = {}
      inputs_string.each_line do |l|
        next if l == "\n"
        a = l.split(' ')
        inputs[a[0]] = a[1].to_i    
      end
      # TODO: reinsert globals line logic
      # We don't want to instrument the instrumentation
      #inputs.delete($globals_line.to_s)
      return inputs

    end

    def parse_input_types ( output )

      types_string = grab_section("Line -> Type of Input Read", output)
      types = {}
      types_string.each_line do |l|
        next if l == "\n"
        a = l.split(' ')
        types[a[0]] = a[1]
      end
      return types

    end

    def relevant_constraint ( constraint, symvars )

      is_relevant = false
      symvars.each do |v|
        if constraint.include? v
          is_relevant = true
          break
        end
      end

      return is_relevant

    end

    def parse_assumptions ( output, symvars_to_line )

      assumptions = grab_section("Control Dependent Slice", output)
      symvars = symvars_to_line.keys
      slice = []
      assumptions.each_line do |a|
        next if (a == "\n" || a.include?("true") || a.include?("!(false)"))
        next unless relevant_constraint(a, symvars)
        slice << a.strip
      end

      symvars_to_array_elem = map_symvars_to_array_elem(symvars_to_line)
      
      symvars_to_array_elem.each do |symvar,array_string|
        slice.each do |a|
          a.gsub!(/(#{symvar}\D|#{symvar}$)/, array_string)
        end
      end

      assumptions = []
      slice.each do |a|
        #assume = a.split(' ',3)[2].strip
        assumptions << a.strip
      end

      return assumptions
      puts "Assumptions: #{assumptions}".red
    end

    def map_symvars_to_array_elem (symvars_to_line_map)
      line_map = symvars_to_line_map.group_by{|k,v| v}
      line_to_symvars = {}
      line_map.each do |line, pairs|
        sym_vars = []
        pairs.each do |p|
          sym_vars << p.first
        end
        line_to_symvars[line] = sym_vars
      end
      map = {}
      
      line_to_symvars.each do |line, symvars|
        symvars.each_with_index do |sym_var, i|
          if sym_var.start_with? '['
            sym_var = sym_var[1..-2] # strip array braces
          end
          raw_read = "INPUTS_at_#{line}[#{i}]"
          map[sym_var] = raw_read

          var = $lines_to_vars[line]
          pretty_read = "#{var.upcase}_#{i}"
          $raw_to_pretty_reads[raw_read] = pretty_read
        end
      end
      return map

    end

    $instrumentation_line = 1

    def instrument ( program, assumptions, symvars_to_lines, input_counts, input_types )

      program = ensure_assume_declaration(program)

      globals = make_globals(input_counts, input_types)
      init_reads_function = make_init_reads_function(input_counts, input_types)
      program = inject_read_instrumentation(program, globals + init_reads_function)
      program = replace_read_expressions(program, input_counts, input_types)
      
      negated_assumptions = negate_assumptions(assumptions)
      program = inject_assumes(program, negated_assumptions)

    end

    def replace_read_expressions ( program, input_table, input_types )
      lines = input_table.keys
      lines.map! { |l| l.to_i }
      lines.each { |line| write_input_read(program,line,input_types) unless line == $instrumentation_line } 
      
      return program
    end

    def write_input_read(fname, at_line, input_types)
      line = at_line
      type = input_types[line.to_s]
      open(fname, 'r+') do |f|
        while (at_line-=1) > 0 # read up to the line you want to write after
          f.readline
        end
        pos = f.pos # save your position (after line to be rewritten) in the file
        line_to_rewrite = f.readline
        # rewrite line using gsub
        input_array_read = "INPUTS_at_#{line}[input_at_#{line}_counter++]"
        raw_read = "__VERIFIER_nondet_#{type}()"
        array_bounds_check = "(input_at_#{line}_counter < num_of_inputs_at_#{line})"
        ternary_expr = "#{array_bounds_check} ? #{input_array_read} : #{raw_read} ;"
        line_to_rewrite.gsub!(/=.*__VERIFIER_nondet_#{Regexp.quote(type)}(\s)*\((\s)*\)(\s)*;/, "= "+ternary_expr)
        rest = f.read # save the rest of the file
        f.seek pos # go back to the old position
        f.write (line_to_rewrite) # write new data
        f.write rest # write rest of file
      end
    end

    def make_globals ( input_counts, input_types )
      globals = ""
      input_counts.each do |line, count|
        unless line == $instrumentation_line 
          type_decl = get_type_declaration(input_types[line])
          globals += "int num_of_inputs_at_#{line} = #{count}; #{type_decl} INPUTS_at_#{line}[#{count}]; int input_at_#{line}_counter = 0;"
        end
      end 

      return globals
    end

    def make_init_reads_function ( input_counts, input_types )
      readlines = ""
      input_counts.each do |line, count|
        unless line == $instrumentation_line
          type = input_types[line]
          readlines += "  for (int instrumentation_index=0; instrumentation_index<#{count}; instrumentation_index++) INPUTS_at_#{line}[instrumentation_index] = __VERIFIER_nondet_#{type}();"
        end
      end
      init_reads_function = "int initialize_reads() {"+readlines+"}"

      return init_reads_function
    end

    def inject_init_reads_setup ( file, str )
      # This is a hack to not have to create a frame file.
      # We just look for the regular expression hardcoded as
      # the beginning of the globals string and end of the
      # init_reads function
      reads_regex = '(int num_of_inputs_.*_index\] = __VERIFIER_nondet_\w+\(\);}|int initialize_reads\(\) {})'
      file_string = File.read(file)
      if file_string =~ /#{reads_regex}/      
        file_string.gsub!(/#{reads_regex}/, str)
      else
        file_string = str + file_string
      end
      File.open(file, 'w') { |f| f.write(file_string) } 

      return file

    end

    def get_type_declaration ( type )

      case type
      when "int"
        return "int"
      when "char"
        return "char"
      when "bool"
        return "_Bool"
      when "float"
        return "float"
      when "double"
        return "double"
      when "long"
        return "long"
      when "short"
        return "short"
      when "size_t"
        return "size_t"
      when "uchar"
        return "unsigned char"
      when "uint"
        return "unsigned int"
      when "ulong"
        return "unsigned long"
      when "unsigned"
        return "unsigned"
      when "ushort"
        return "unsigned short"
      else
        message = "Unexpected type: #{type}. Aborting."
        assert { puts message; $log.write message; false }
      end

    end

    def ensure_assume_declaration ( file )
      assume_declaration = "extern void __VERIFIER_assume(int);"
      assume_regex = 'extern\s*void\s*__VERIFIER_assume\s*\(\s*int\s*\)\s*;'
      file_string = File.read(file)
      unless file_string =~ /#{assume_regex}/
        file_string = assume_declaration + file_string
        File.open(file, 'w') { |f| f.write(file_string) } 
      end

      return file
    end

    def inject_read_instrumentation ( file, str )
      file = inject_init_reads_setup(file, str)
      file = inject_call_to_init_reads( file )
    end

    def inject_call_to_init_reads ( file )
      main_regex = '((int\s)*main\s*\(\s*(void)*\s*\)\s*{)'
      file_string = File.read(file)

      unless file_string.include? 'initialize_reads();'
        file_string.gsub!(/#{main_regex}/, '\1'+'initialize_reads();')
        File.open(file, 'w') { |f| f.write(file_string) } 
      end

      return file
    end

    def negate_assumptions ( pc_list )

      if pc_list.any?
        assume_statement = ""
        pc_list.each do |pc|

          sanitized_pc = []
          pc.each do |expr|

            if contains_subtraction(expr)
              expr = rewrite_subtract_expression(expr)
            end
        
            if contains_geq(expr)
              expr = rewrite_geq_expression(expr) 
            end

            if contains_leq(expr)
              expr = rewrite_leq_expression(expr) 
            end

            sanitized_pc << expr
          end
       
          conjuncted_pc = "(#{sanitized_pc.to_a.join(" && ")})"
          puts "A blocking clause: #{conjuncted_pc}".red if $debug
          assume_statement += "__VERIFIER_assume(!(#{conjuncted_pc}));"
        end
      end

      return assume_statement

    end

    def inject_assumes ( file, assumptions )
      error_regex = '^(?!.*extern void(\s)*__VERIFIER_error)(?!.*ERROR:)(^[^\__VERIFIER]*).*__VERIFIER_error'
      error_regex_with_numbered_label = '^(?!.*extern void(\s)*__VERIFIER_error)(\s*error_(\d+):).*__VERIFIER_error'
      error_regex_with_error_label = '^(?!.*extern void(\s)*__VERIFIER_error)(\s*ERROR:).*__VERIFIER_error'
      assume_file_string = File.read(file)

      if assume_file_string =~ /\s*ERROR:/
        assume_file_string.gsub!(/#{error_regex_with_error_label}/, '\2'+assumptions+"__VERIFIER_error")
      end

      if assume_file_string =~ /(\s*)error_(\d+):/
        assume_file_string.gsub!(/#{error_regex_with_numbered_label}/, '\2'+assumptions+"__VERIFIER_error")
      else
        assume_file_string.gsub!(/#{error_regex}/, '\2'+assumptions+"__VERIFIER_error")
      end

      File.open(file, 'w') { |f| f.write(assume_file_string) }
      return file
    end

    # Input:  SARL-generated expression
    # Output: Equivalent expression without
    #         subtraction subexpressions
    def rewrite_subtract_expression ( orig_expr ) 
      expr = orig_expr.dup
      
      disjuncts = []
      expr.split(' || ').each do |orig_sub_expr|
        sub_expr = orig_sub_expr.dup
        if sub_expr.start_with? '('
          sub_expr = sub_expr[1..-2]
        end
        right_operands = right_subtract_operands(sub_expr)
        to_remove = subtract_subexpressions_to_remove(sub_expr)
        to_remove.each {|s| sub_expr.slice! s}
        sub_expr.strip!
      
        addition_subexpression = right_operands.join(' + ')

        if (sub_expr.start_with? '0')
          disjuncts << "(#{addition_subexpression} + #{sub_expr})"
        elsif (sub_expr.end_with? '0')
          disjuncts << "(#{sub_expr} + #{addition_subexpression})"
        else
          puts "** Found: #{sub_expr}".red
          puts 'Not an expected SARL expression. Aborting.'
          abort
        end
      end
      return '('+disjuncts.join(' || ')+')'
    end

    def rewrite_geq_expression ( orig_expr )
      return orig_expr.gsub(/>=/,"+ 1 >")
    end

    def rewrite_leq_expression ( orig_expr )
      return orig_expr.gsub(/<=/,"< 1 +")
    end

    def first_char (str)
      return str[0]
    end

    def last_char (str)
      return str[-1]
    end

    # Input:  SARL-generated expression
    # Output: Array of strings of the right
    #         operand of a subtract expression
    def right_subtract_operands ( expr )
      match_strings = expr.scan(/(- (\S*)|- (\S*)$)/)
      right_operands = []
      match_strings.map {|s| right_operands << s[1]}
      return right_operands
    end

    # Input:  SARL-generated expression
    # Output: Array of strings of minus OP symbol
    #         and its right operand
    def subtract_subexpressions_to_remove ( expr )
      match_strings = expr.scan(/(- (\S*)|- (\S*)$)/)
      subexpressions_to_delete = []
      match_strings.map {|s| subexpressions_to_delete << s[0]}
      return subexpressions_to_delete 
    end

    def contains_subtraction ( expr )
      return expr.include? ' - '
    end

    def contains_geq ( expr )
      return expr.include? ' >= '
    end

    def contains_leq ( expr )
      return expr.include? ' <= '
    end

    def get_candidate_powerset ( clauses )
      p = powerset(clauses.clone)
      puts "Reached past powerset call; the powerset is: #{p}".blue
      # Sort list; begin by dropping single
      # clauses, then pairwise, etc.
      p.sort_by!(&:length)
      p.shift # Drop the empty set at the head
      return p
    end

    # Solution take from:
    # http://stackoverflow.com/questions/2779094/
    # what-algorithm-can-calculate-the-power-set-of-a-given-set
    def powerset(set)
      puts "Calling powerset function".red
      return [set] if set.empty?

      p = set.pop
      subset = powerset(set)  
      subset | subset.map { |x| x | [p] }
    end

    def find_maximally_intersecting_set(set_of_sets, set)
      intersections = set_of_sets.map {|s| s & set}
      if intersections.empty?
        return []
      else
        return intersections.group_by(&:size).max.last.flatten.uniq
      end
    end

    class Array
      def is_a_subset_of ( other_array )
        return (self - other_array).empty?
      end
    end

    def update_disjoint_covering ( disjoint_covering, generalized_clause, log )

      log.write "\n  ++ Updating disjoint covering map ++\n"

      elements_covered_by_generalization = []
      disjoint_covering.each do |u, image_of_u|
        if generalized_clause.is_a_subset_of u
          elements_covered_by_generalization << u
        end
      end

      image_of_generalized_clause = [$last_blocking_pc]
      elements_covered_by_generalization.each do |e|
        image_of_generalized_clause.concat disjoint_covering[e]
      end

      elements_covered_by_generalization.each do |u|
        log.write "  Removing #{u} from the upper approximation"
        disjoint_covering.delete u
      end
      
      image_of_generalized_clause.uniq!
      disjoint_covering[generalized_clause] = image_of_generalized_clause.clone
      log.write "  The image of #{generalized_clause} is:\n"\
                "    #{image_of_generalized_clause}\n"

    end

    def make_bracketed_pairs ( disjoint_covering )
      str = ""
      disjoint_covering.each do |upper, lower|
        str += "  < #{upper} , #{lower} >\n"
      end
      return str
    end

    # Solution taken from:
    # http://stackoverflow.com/questions/19595840/rails-get-the-time-difference-in-hours-minutes-and-seconds
    def time_diff(start_time, end_time)
      seconds_diff = (start_time - end_time).to_i.abs

      hours = seconds_diff / 3600
      seconds_diff -= hours * 3600

      minutes = seconds_diff / 60
      seconds_diff -= minutes * 60

      seconds = seconds_diff

      "#{hours.to_s.rjust(2, '0')}:#{minutes.to_s.rjust(2, '0')}:#{seconds.to_s.rjust(2, '0')}"
  end

    def prettify ( str )
      pretty_string = str.dup
      $raw_to_pretty_reads.each do |raw_read, pretty_read|
        if pretty_string.include? raw_read
          pretty_string.gsub! raw_read, pretty_read
        end
      end
      pretty_string.gsub! "1*", "" # Remove multiplication by unity
      pretty_string.gsub! "(int)", "" # Remove int casts
      pretty_string.gsub! "(real)", "" # Remove real casts
      return pretty_string
    end

      class AssertionError < RuntimeError
      end

      def assert &block
        raise AssertionError unless yield
      end

      def get_bare_name ( program )
        # Drop the extension after the *final* period
        name_without_ext = program.split('.')[0..-2].join('.')
        bare_name = name_without_ext.split('/').last
        return bare_name
      end

      def prepend_error_at_line ( file, target_line )

        curr_line = 1
        lines = IO.readlines(file).map do |line|
          if curr_line == target_line
            line = '__VERIFIER_error(); //' + line
          end
          curr_line += 1
          line
        end
         
        File.open(file, 'w') do |f|
          f.puts lines
        end

      end

      def rewrite_line_with_string ( file, target_line, string )

        curr_line = 1
        lines = IO.readlines(file).map do |line|
          if curr_line == target_line
            line = string
          end
          curr_line += 1
          line
        end
         
        File.open(file, 'w') do |f|
          f.puts lines
        end

      end

      # The canonical string for an abstract witness will
      # be space delimited tuples of:
      #   (branch, branch_outcome)
      def get_canonical_string (witness)

        string = ""

        witness.branch_lines.each_with_index do |branch, i|
          outcome = witness.outcomes[i]
          string += "(#{branch},#{outcome}) "
        end

        return string

      end

      def get_abstract_witness (witness_graphml, is_cpa)

        graphml_file = File.read(witness_graphml)
        witness = Nokogiri::XML.parse(graphml_file)

        guidance = Guidance.new

        branches = get_branches_in_error_path(witness, is_cpa)
        guidance.branch_lines = branches.map { |b| b.line_number }
        guidance.outcomes = branches.map { |b| b.branch_taken }

        return guidance

      end

      def is_relevant_control_edge (edge)
        sourcecode = "data[key='sourcecode']"

        if edge.values.include? "sink"
          return false
        elsif edge.css(sourcecode).text.include? "instrumentation_index"
          return false
        elsif edge.css(sourcecode).text.include? "input_at_"
          return false
        elsif edge.css(sourcecode).text.include? "__VERIFIER_assume("
          return false
        elsif edge.css(sourcecode).text.include? "INPUTS_at_"
          return false
        elsif edge.css(sourcecode).text.include? "__CPAchecker_TMP"
          return false
        else
          return true
        end
      end

      def get_branches_in_error_path ( witness, is_cpa )

        branch_trace = []

        is_loop_head = false
        witness.css('edge').each do |edge|
          if edge.at("data[key='enterLoopHead']")
            is_loop_head = true
            next
          end
          if edge.at("data[key='control']")
            if is_relevant_control_edge(edge)
              branch_trace << Branch.new(edge, is_loop_head)
            end
          end
          is_loop_head = false
        end

        if is_cpa
            branches_to_remove = []
            
            branch_trace.each_cons(2) do |curr_b, next_b|
              if ((curr_b.line_number == next_b.line_number) &&
                  (!curr_b.is_loop_head))
                branches_to_remove << curr_b
              end
            end  

            branch_trace = branch_trace - branches_to_remove

        end

        return branch_trace

      end

      def make_guidance_file ( guidance, dir, program_name )

        guidance_file = "#{dir}/directives"
        program_name = program_name.split('/')[-1]

        File.open(guidance_file, 'w') do |f|
          f.puts program_name
          f.puts "lines #{guidance.branch_lines.join(' ')}"
          f.puts "guide #{guidance.outcomes.join(' ')}"
        end

        return guidance_file

      end

      def result_string ( result )
        case result
        when 0 then "unknown"
        when 1 then "safe"
        when 2 then "error found"
        when 3 then "cached"
        else assert { puts 'Illegal result'.red; false }
        end
      end

      def svcomp_error_found ( civl_output )
        return civl_output.include? '$assert(0, "__VERIFIER_error() is called.\n")'
      end

      def timed_out ( civl_output )
        return civl_output.include? 'Time out.'
      end

      def no_relevant_error_found ( civl_output )
        if civl_output.include? 'The standard properties hold for all executions.'
          return true
        elsif civl_output.include? 'The program MAY NOT be correct.'
          # If we got here and 'VERIFIER_error' was not in the results, then either only picky CIVL errors were caught, the error bound was exceeded, or both.
          return true
        else
          return false
        end
      end

      def is_svcomp_error( violation_summary )
        return violation_summary.include? "__VERIFIER_error() is called."
      end

      def extract_trace_id ( violation_summary )
        trace_id_regex = 'Violation (\d) encountered at depth .*'
        return violation_summary[/#{trace_id_regex}/,1]
      end

      def update_symvars_to_lines ( curr_map, new_map )
        new_map.each do |line, var|
          curr_map[line] = var
        end
        return curr_map
      end

      def update_input_counts ( curr_map, new_map )
        instr_line = "1"
        new_map.each do |line, count|
          next if line == instr_line
          if curr_map.keys.include? line
            curr_map[line] += count
          else
            curr_map[line] = count
          end
        end
        return curr_map
      end

      def update_input_types ( curr_map, new_map )
        instr_line = "1"
        new_map.each do |line, type|
          next if line == instr_line
          curr_map[line] = type
        end
        return curr_map
      end

      def update_lines_to_vars ( curr_map, new_map )
        new_map.each do |line, var|
          curr_map[line] = var
        end
        return curr_map
      end

      def grab_section ( name, output )
        start = "BEGIN #{name}\n"
        finish = "END #{name}\n"
        return output[/#{start}(.*?)#{finish}/m, 1]
      end

      def parse_read_calls ( output )

        map_string = grab_section("SymVar -> Line -> Var", output)
        symvars_to_line = []

        map_string.each_line do |l|

          next if l == "\n"

          triple = l.strip.split(' ',3)
          line = triple[1]; lhs = triple[2]
       
          instrumentation_line = "1"
          next if line == instrumentation_line

          symvars_to_line << triple[0..1] 
          
        end

        symvars_to_line = Hash[symvars_to_line]
        return symvars_to_line

      end

      def get_number_sliced_away ( output )
        return grab_section("Number of Conjuncts Sliced Away", output).to_i
      end

      def make_line_to_var_map ( output )

        map_string = grab_section("SymVar -> Line -> Var", output)
        lines_to_vars = []
        map_string.each_line do |l|
          next if l == "\n"

          triple = l.strip.split(' ',3)
          lhs = triple[2]
          unless lhs =~ /.*INPUTS_at_(\d+).*/
            lines_to_vars << triple[1..2] 
          end
        end
        lines_to_vars = Hash[lines_to_vars]

        return lines_to_vars

      end

      def parse_instrument_lines ( output )
        lines_string = grab_section("Guidance Lines", output)
        lines = []
        lines_string.each_line {|l| next if l == "\n"; lines << l.strip}
        puts "Guidance Lines: #{lines}" if $debug
        return lines
      end

      def parse_input_frequency ( output )

        inputs_string = grab_section("Line -> Number of Input Reads", output)
        inputs = {}
        inputs_string.each_line do |l|
          next if l == "\n"
          a = l.split(' ')
          inputs[a[0]] = a[1].to_i    
        end
        # TODO: reinsert globals line logic
        # We don't want to instrument the instrumentation
        #inputs.delete($globals_line.to_s)
        return inputs

      end

      def parse_input_types ( output )

        types_string = grab_section("Line -> Type of Input Read", output)
        types = {}
        types_string.each_line do |l|
          next if l == "\n"
          a = l.split(' ')
          types[a[0]] = a[1]
        end
        return types

      end

      def relevant_constraint ( constraint, symvars )

        is_relevant = false
        symvars.each do |v|
          if constraint.include? v
            is_relevant = true
            break
          end
        end

        return is_relevant

      end

      def parse_assumptions ( output, symvars_to_line )

        assumptions = grab_section("Control Dependent Slice", output)
        symvars = symvars_to_line.keys
        slice = []
        assumptions.each_line do |a|
          next if (a == "\n" || a.include?("true") || a.include?("!(false)"))
          next unless relevant_constraint(a, symvars)
          slice << a.strip
        end

        symvars_to_array_elem = map_symvars_to_array_elem(symvars_to_line)
        
        symvars_to_array_elem.each do |symvar,array_string|
          slice.each do |a|
            a.gsub!(/(#{symvar}\D|#{symvar}$)/, array_string)
          end
        end

        assumptions = []
        slice.each do |a|
          #assume = a.split(' ',3)[2].strip
          assumptions << a.strip
        end

        return assumptions
        puts "Assumptions: #{assumptions}".red
      end

      def map_symvars_to_array_elem (symvars_to_line_map)
        line_map = symvars_to_line_map.group_by{|k,v| v}
        line_to_symvars = {}
        line_map.each do |line, pairs|
          sym_vars = []
          pairs.each do |p|
            sym_vars << p.first
          end
          line_to_symvars[line] = sym_vars
        end
        map = {}
        
        line_to_symvars.each do |line, symvars|
          symvars.each_with_index do |sym_var, i|
            if sym_var.start_with? '['
              sym_var = sym_var[1..-2] # strip array braces
            end
            raw_read = "INPUTS_at_#{line}[#{i}]"
            map[sym_var] = raw_read

            var = $lines_to_vars[line]
            pretty_read = "#{var.upcase}_#{i}"
            $raw_to_pretty_reads[raw_read] = pretty_read
          end
        end
        return map

      end

      $instrumentation_line = 1

      def instrument ( program, assumptions, symvars_to_lines, input_counts, input_types )

        program = ensure_assume_declaration(program)

        globals = make_globals(input_counts, input_types)
        init_reads_function = make_init_reads_function(input_counts, input_types)
        program = inject_read_instrumentation(program, globals + init_reads_function)
        program = replace_read_expressions(program, input_counts, input_types)
        
        negated_assumptions = negate_assumptions(assumptions)
        program = inject_assumes(program, negated_assumptions)

      end

      def replace_read_expressions ( program, input_table, input_types )
        lines = input_table.keys
        lines.map! { |l| l.to_i }
        lines.each { |line| write_input_read(program,line,input_types) unless line == $instrumentation_line } 
        
        return program
      end

      def write_input_read(fname, at_line, input_types)
        line = at_line
        type = input_types[line.to_s]
        open(fname, 'r+') do |f|
          while (at_line-=1) > 0 # read up to the line you want to write after
            f.readline
          end
          pos = f.pos # save your position (after line to be rewritten) in the file
          line_to_rewrite = f.readline
          # rewrite line using gsub
          input_array_read = "INPUTS_at_#{line}[input_at_#{line}_counter++]"
          raw_read = "__VERIFIER_nondet_#{type}()"
          array_bounds_check = "(input_at_#{line}_counter < num_of_inputs_at_#{line})"
          ternary_expr = "#{array_bounds_check} ? #{input_array_read} : #{raw_read} ;"
          line_to_rewrite.gsub!(/=.*__VERIFIER_nondet_#{Regexp.quote(type)}(\s)*\((\s)*\)(\s)*;/, "= "+ternary_expr)
          rest = f.read # save the rest of the file
          f.seek pos # go back to the old position
          f.write (line_to_rewrite) # write new data
          f.write rest # write rest of file
        end
      end

      def make_globals ( input_counts, input_types )
        globals = ""
        input_counts.each do |line, count|
          unless line == $instrumentation_line 
            type_decl = get_type_declaration(input_types[line])
            globals += "int num_of_inputs_at_#{line} = #{count}; #{type_decl} INPUTS_at_#{line}[#{count}]; int input_at_#{line}_counter = 0;"
          end
        end 

        return globals
      end

      def make_init_reads_function ( input_counts, input_types )
        readlines = ""
        input_counts.each do |line, count|
          unless line == $instrumentation_line
            type = input_types[line]
            readlines += "  for (int instrumentation_index=0; instrumentation_index<#{count}; instrumentation_index++) INPUTS_at_#{line}[instrumentation_index] = __VERIFIER_nondet_#{type}();"
          end
        end
        init_reads_function = "int initialize_reads() {"+readlines+"}"

        return init_reads_function
      end

      def inject_init_reads_setup ( file, str )
        # This is a hack to not have to create a frame file.
        # We just look for the regular expression hardcoded as
        # the beginning of the globals string and end of the
        # init_reads function
        reads_regex = '(int num_of_inputs_.*_index\] = __VERIFIER_nondet_\w+\(\);}|int initialize_reads\(\) {})'
        file_string = File.read(file)
        if file_string =~ /#{reads_regex}/      
          file_string.gsub!(/#{reads_regex}/, str)
        else
          file_string = str + file_string
        end
        File.open(file, 'w') { |f| f.write(file_string) } 

        return file

      end

      def get_type_declaration ( type )

        case type
        when "int"
          return "int"
        when "char"
          return "char"
        when "bool"
          return "_Bool"
        when "float"
          return "float"
        when "double"
          return "double"
        when "long"
          return "long"
        when "short"
          return "short"
        when "size_t"
          return "size_t"
        when "uchar"
          return "unsigned char"
        when "uint"
          return "unsigned int"
        when "ulong"
          return "unsigned long"
        when "unsigned"
          return "unsigned"
        when "ushort"
          return "unsigned short"
        else
          message = "Unexpected type: #{type}. Aborting."
          assert { puts message; $log.write message; false }
        end

      end

      def ensure_assume_declaration ( file )
        assume_declaration = "extern void __VERIFIER_assume(int);"
        assume_regex = 'extern\s*void\s*__VERIFIER_assume\s*\(\s*int\s*\)\s*;'
        file_string = File.read(file)
        unless file_string =~ /#{assume_regex}/
          file_string = assume_declaration + file_string
          File.open(file, 'w') { |f| f.write(file_string) } 
        end

        return file
      end

      def inject_read_instrumentation ( file, str )
        file = inject_init_reads_setup(file, str)
        file = inject_call_to_init_reads( file )
      end

      def inject_call_to_init_reads ( file )
        main_regex = '((int\s)*main\s*\(\s*(void)*\s*\)\s*{)'
        file_string = File.read(file)

        unless file_string.include? 'initialize_reads();'
          file_string.gsub!(/#{main_regex}/, '\1'+'initialize_reads();')
          File.open(file, 'w') { |f| f.write(file_string) } 
        end

        return file
      end

      def negate_assumptions ( pc_list )

        if pc_list.any?
          assume_statement = ""
          pc_list.each do |pc|

            sanitized_pc = []
            pc.each do |expr|

              if contains_subtraction(expr)
                expr = rewrite_subtract_expression(expr)
              end
          
              if contains_geq(expr)
                expr = rewrite_geq_expression(expr) 
              end

              if contains_leq(expr)
                expr = rewrite_leq_expression(expr) 
              end

              sanitized_pc << expr
            end
         
            conjuncted_pc = "(#{sanitized_pc.to_a.join(" && ")})"
            puts "A blocking clause: #{conjuncted_pc}".red if $debug
            assume_statement += "__VERIFIER_assume(!(#{conjuncted_pc}));"
          end
        end

        return assume_statement

      end

      def inject_assumes ( file, assumptions )
        error_regex = '^(?!.*extern void(\s)*__VERIFIER_error)(?!.*ERROR:)(^[^\__VERIFIER]*).*__VERIFIER_error'
        error_regex_with_numbered_label = '^(?!.*extern void(\s)*__VERIFIER_error)(\s*error_(\d+):).*__VERIFIER_error'
        error_regex_with_error_label = '^(?!.*extern void(\s)*__VERIFIER_error)(\s*ERROR:).*__VERIFIER_error'
        assume_file_string = File.read(file)

        if assume_file_string =~ /\s*ERROR:/
          assume_file_string.gsub!(/#{error_regex_with_error_label}/, '\2'+assumptions+"__VERIFIER_error")
        end

        if assume_file_string =~ /(\s*)error_(\d+):/
          assume_file_string.gsub!(/#{error_regex_with_numbered_label}/, '\2'+assumptions+"__VERIFIER_error")
        else
          assume_file_string.gsub!(/#{error_regex}/, '\2'+assumptions+"__VERIFIER_error")
        end

        File.open(file, 'w') { |f| f.write(assume_file_string) }
        return file
      end

      # Input:  SARL-generated expression
      # Output: Equivalent expression without
      #         subtraction subexpressions
      def rewrite_subtract_expression ( orig_expr ) 
        expr = orig_expr.dup
        
        disjuncts = []
        expr.split(' || ').each do |orig_sub_expr|
          sub_expr = orig_sub_expr.dup
          if sub_expr.start_with? '('
            sub_expr = sub_expr[1..-2]
          end
          right_operands = right_subtract_operands(sub_expr)
          to_remove = subtract_subexpressions_to_remove(sub_expr)
          to_remove.each {|s| sub_expr.slice! s}
          sub_expr.strip!
        
          addition_subexpression = right_operands.join(' + ')

          if (sub_expr.start_with? '0')
            disjuncts << "(#{addition_subexpression} + #{sub_expr})"
          elsif (sub_expr.end_with? '0')
            disjuncts << "(#{sub_expr} + #{addition_subexpression})"
          else
            puts "** Found: #{sub_expr}".red
            puts 'Not an expected SARL expression. Aborting.'
            abort
          end
        end
        return '('+disjuncts.join(' || ')+')'
      end

      def rewrite_geq_expression ( orig_expr )
        return orig_expr.gsub(/>=/,"+ 1 >")
      end

      def rewrite_leq_expression ( orig_expr )
        return orig_expr.gsub(/<=/,"< 1 +")
      end

      def first_char (str)
        return str[0]
      end

      def last_char (str)
        return str[-1]
      end

      # Input:  SARL-generated expression
      # Output: Array of strings of the right
      #         operand of a subtract expression
      def right_subtract_operands ( expr )
        match_strings = expr.scan(/(- (\S*)|- (\S*)$)/)
        right_operands = []
        match_strings.map {|s| right_operands << s[1]}
        return right_operands
      end

      # Input:  SARL-generated expression
      # Output: Array of strings of minus OP symbol
      #         and its right operand
      def subtract_subexpressions_to_remove ( expr )
        match_strings = expr.scan(/(- (\S*)|- (\S*)$)/)
        subexpressions_to_delete = []
        match_strings.map {|s| subexpressions_to_delete << s[0]}
        return subexpressions_to_delete 
      end

      def contains_subtraction ( expr )
        return expr.include? ' - '
      end

      def contains_geq ( expr )
        return expr.include? ' >= '
      end

      def contains_leq ( expr )
        return expr.include? ' <= '
      end

      def get_candidate_powerset ( clauses )
        p = powerset(clauses.clone)
        puts "Reached past powerset call; the powerset is: #{p}".blue
        # Sort list; begin by dropping single
        # clauses, then pairwise, etc.
        p.sort_by!(&:length)
        p.shift # Drop the empty set at the head
        return p
      end

      # Solution take from:
      # http://stackoverflow.com/questions/2779094/
      # what-algorithm-can-calculate-the-power-set-of-a-given-set
      def powerset(set)
        puts "Calling powerset function".red
        return [set] if set.empty?

        p = set.pop
        subset = powerset(set)  
        subset | subset.map { |x| x | [p] }
      end

      def find_maximally_intersecting_set(set_of_sets, set)
        intersections = set_of_sets.map {|s| s & set}
        if intersections.empty?
          return []
        else
          return intersections.group_by(&:size).max.last.flatten.uniq
        end
      end

      class Array
        def is_a_subset_of ( other_array )
          return (self - other_array).empty?
        end
      end

      def update_disjoint_covering ( disjoint_covering, generalized_clause, log )

        log.write "\n  ++ Updating disjoint covering map ++\n"

        elements_covered_by_generalization = []
        disjoint_covering.each do |u, image_of_u|
          if generalized_clause.is_a_subset_of u
            elements_covered_by_generalization << u
          end
        end

        image_of_generalized_clause = [$last_blocking_pc]
        elements_covered_by_generalization.each do |e|
          image_of_generalized_clause.concat disjoint_covering[e]
        end

        elements_covered_by_generalization.each do |u|
          log.write "  Removing #{u} from the upper approximation"
          disjoint_covering.delete u
        end
        
        image_of_generalized_clause.uniq!
        disjoint_covering[generalized_clause] = image_of_generalized_clause.clone
        log.write "  The image of #{generalized_clause} is:\n"\
                  "    #{image_of_generalized_clause}\n"

      end

      def make_bracketed_pairs ( disjoint_covering )
        str = ""
        disjoint_covering.each do |upper, lower|
          str += "  < #{upper} , #{lower} >\n"
        end
        return str
      end

      # Solution taken from:
      # http://stackoverflow.com/questions/19595840/rails-get-the-time-difference-in-hours-minutes-and-seconds
      def time_diff(start_time, end_time)
        seconds_diff = (start_time - end_time).to_i.abs

        hours = seconds_diff / 3600
        seconds_diff -= hours * 3600

        minutes = seconds_diff / 60
        seconds_diff -= minutes * 60

        seconds = seconds_diff

        "#{hours.to_s.rjust(2, '0')}:#{minutes.to_s.rjust(2, '0')}:#{seconds.to_s.rjust(2, '0')}"
    end

      def prettify ( str )
        pretty_string = str.dup
        $raw_to_pretty_reads.each do |raw_read, pretty_read|
          if pretty_string.include? raw_read
            pretty_string.gsub! raw_read, pretty_read
          end
        end
        pretty_string.gsub! "1*", "" # Remove multiplication by unity
        pretty_string.gsub! "(int)", "" # Remove int casts
        pretty_string.gsub! "(real)", "" # Remove real casts
        return pretty_string
      end


        # This hash will hold all of the options
        # parsed from the command-line by
        # OptionParser.
        $options = {}

        optparse = OptionParser.new do|opts|
          # Set a banner, displayed at the top
          # of the help screen.
          opts.banner = "\n  Usage: cfc.rb [options] foo.c\n\n"

          # Define the options, and what they do
          $options[:verbose] = false
          opts.on( '-v', '--verbose', 'Output more information' ) do
            $options[:verbose] = true
          end

          $options[:full] = false 
          opts.on( '-f', '--full', 'Allow all CPA configs to run to completion' ) do
            $options[:full] = true
          end

          # This displays the help screen, all programs are
          # assumed to have this option.
          opts.on( '-h', '--help', 'Display this screen' ) do
            puts opts
            exit
          end

          $options[:only_cpa] = false
          opts.on( '--only_cpa', 'Only run CPA as the overapproximator' ) do
            $options[:only_cpa] = true
          end

          $options[:single_cpa] = false
          opts.on( '--single_cpa', 'Only run CPA config svcomp17 as the overapproximator' ) do
            $options[:only_cpa] = true
            $options[:single_cpa] = true
          end

          $options[:only_ua] = false
          opts.on( '--only_ua', 'Only run UA as the overapproximator' ) do
            $options[:only_ua] = true
          end

          $options[:civl_timeout] = 60 # One minute default
          opts.on( '--civl_timeout BOUND', 'Set CIVL time bound (in seconds)' ) do |b|
            $options[:civl_timeout] = b
          end

          $options[:cpa_timeout] = 60 # One minute default
          opts.on( '--cpa_timeout BOUND', 'Set CPA time bound (in seconds)' ) do |b|
            $options[:cpa_timeout] = b
          end

          $options[:ua_timeout] = 60 # One minute default
          opts.on( '--ua_timeout BOUND', 'Set UA time bound (in seconds)' ) do |b|
            $options[:ua_timeout] = b
          end

          $options[:first_tool] = "" # Empty default
          opts.on( '--first_tool TOOL', 'Force one tool to be run first' ) do |b|
            $options[:first_tool] = b
          end

        end

        # Parse the command-line. There are two forms
        # of the parse method. The 'parse' method simply parses
        # ARGV, while the 'parse!' method parses ARGV and removes
        # any options found there, as well as any parameters for
        # the options. 
        optparse.parse!

      program = ARGV[0] # C program we are analyzing

        cfc_dir = "#{Dir.pwd}/analysis_logs"
        `mkdir #{cfc_dir}` unless File.exist? cfc_dir

        program_name = get_bare_name(program)
        program_dir = "#{cfc_dir}/#{program_name}"
        `mkdir #{program_dir}` unless File.exist? program_dir
        `rm -rf #{program_dir}/*` # Clean out possible previous files

        dump_dir = "#{Dir.pwd}/dump"
        `mkdir #{dump_dir}` unless File.exist? dump_dir

      #  instrumented_program = "#{program_dir}/#{program_name}.instrument.c"
      #  `cp #{program} #{instrumented_program}`
         over_instr_program = "#{program_dir}/#{program_name}.over.instr.c"
         under_instr_program = "#{program_dir}/#{program_name}.under.instr.c"
        `cp #{program} #{over_instr_program}`
        `cp #{program} #{under_instr_program}` 

        cpa_output_dir = "#{program_dir}/cpa_output"
        `mkdir #{cpa_output_dir}` unless File.exist? cpa_output_dir


      log_file = "#{program_dir}/LOG"

      class Log
        attr_accessor :log_file

        def initialize ( log_file )
          @log_file = log_file
          `rm -rf #{@log_file}` if File.exist? log_file
        end

        def write ( str )
          str = prettify(str)
          File.open(@log_file,'a') {|f| f.write(str+"\n")}
        end

        def write_banner ( iteration )
          banner = "\n================= ITERATION #{iteration} ===================\n"
          write(banner)
        end
      end

      log = Log.new(log_file)

        disjoint_covering = {}
        configurations = []
        if $options[:first_tool].empty?
          configurations = [
            ["UltimateAutomizer"],
            ["CPA", "sv-comp17"],
            ["CPA", "sv-comp16-bam"],
            ["CPA", "sv-comp16--k-induction"]
          ]
        else
          case $options[:first_tool]
          when "uautomizer"
            configurations = [
              ["UltimateAutomizer"],
              ["CPA", "sv-comp17"],
              ["CPA", "sv-comp16-bam"],
              ["CPA", "sv-comp16--k-induction"]
            ]
          when "cpa-seq"
            configurations = [
              ["CPA", "sv-comp17"],
              ["CPA", "sv-comp16-bam"],
              ["CPA", "sv-comp16--k-induction"],
              ["UltimateAutomizer"]
            ]
          when "cpa-bam-bnb"
            configurations = [
              ["CPA", "sv-comp16-bam"],
              ["CPA", "sv-comp17"],
              ["CPA", "sv-comp16--k-induction"],
              ["UltimateAutomizer"]
            ]
          when "cpa-kind"
            configurations = [
              ["CPA", "sv-comp16--k-induction"],
              ["CPA", "sv-comp17"],
              ["CPA", "sv-comp16-bam"],
              ["UltimateAutomizer"]
            ]
          end
        end
        witness_cache = []

        overapprox_result = unknown
        underapprox_result = unknown

        analysis_stopwatch = Stopwatch.new
        possible_failure_stopwatch = Stopwatch.new
        generalization_stopwatch = Stopwatch.new
        definite_failure_stopwatch = Stopwatch.new
        slice_stopwatch = Stopwatch.new

        iteration = 1
        log.write_banner(iteration)
        analysis_stopwatch.start

      loop do
          if just_generalized
            just_generalized = false
          else
            possible_failure_stopwatch.start
            
              goto_beginning_of_analysis_loop = false

              configurations.each do |conf|
                if conf.first == "UltimateAutomizer"
                  if $options[:only_cpa]
                    next
                  else
                      ua_cmd = "#{ua_script} "\
                               "--full-output "\
                               "--spec #{ua_specification} "\
                               "--architecture 32bit "\
                               "--file #{over_instr_program} "\
                               "--witness-dir #{program_dir}"
                      ua_output = `timeout #{$options[:ua_timeout]} #{ua_cmd}`
                      #puts ua_output.yellow
                      if $?.exitstatus == TIMEOUT_STATUS
                        ua_timeouts += 1
                      end
                      File.open("#{program_dir}/Ultimate.log.#{iteration}","w") {|f| f.write(ua_output)}
                      
                      unsupported_operation_message = "- UnsupportedSyntaxResult [Line: "
                      if ua_output.include? unsupported_operation_message
                          instr_program_with_problematic_line = "#{under_instr_program}.problematic.#{iteration}.c"
                          `cp #{under_instr_program} #{instr_program_with_problematic_line}`


                        generalization_stopwatch.start
                            problem_line = ua_output.scan(/UnsupportedSyntaxResult \[Line: (\d+)\]/).flatten.first.to_i
                            message = "  UA found a problem line at: #{problem_line}; prepending error line to instrumented file."
                            log.write message
                            prepend_error_at_line(over_instr_program, problem_line)
                            prepend_error_at_line(under_instr_program, problem_line)
                            generalization_count += 1
                              ua_output = `timeout #{$options[:ua_timeout]} #{ua_cmd}`
                              if $?.exitstatus == TIMEOUT_STATUS
                                ua_timeouts += 1
                              end
                              File.open("#{program_dir}/Ultimate.log.#{iteration}","w") {|f| f.write(ua_output)}  
                                if ua_output.include? "SyntaxErrorResult [Line:"
                                  message = "UltimateAutomizer reported a syntax error. Aborting."
                                  log.write message
                                  assert { puts message.red; false }
                                end

                                if ua_output.include? "Result:\nTRUE"
                                  overapprox_result = safe
                                elsif ua_output.include? "Result:\nFALSE"
                                    witness = "#{program_dir}/witness.graphml"
                                    is_cpa = false
                                    abstract_witness_object = get_abstract_witness(witness, is_cpa)
                                    abstract_witness = get_canonical_string(abstract_witness_object)

                                  if witness_cache.include? abstract_witness
                                    overapprox_result = cached 
                                  else
                                    overapprox_result = error_found 
                                    witness_cache << abstract_witness
                                      guidance_file = make_guidance_file(abstract_witness_object, program_dir, under_instr_program)

                                  end
                                else
                                  overapprox_result = unknown
                                end

                                if conf.first == "UltimateAutomizer"
                                  message = "  UAutomizer result: #{result_string(overapprox_result)}"
                                else
                                  message = "  #{c} result: #{result_string(overapprox_result)}"
                                end
                                log.write message



                        # Do I classify this chunk as a definite failure? It's a definite overapproximation...
                              bound = 42
                              civl_verify_cmd = "#{civl_jar} verify "\
                                                "-svcomp16 "\
                                                "-timeout=#{$options[:civl_timeout]} "\
                                                "-errorBound=#{bound} "\
                                                "-direct=#{guidance_file} "\
                                                "#{under_instr_program}"


                            definite_failure_stopwatch.start
                            civl_output = `#{civl_verify_cmd}`
                            definite_failure_stopwatch.stop

                            #puts "CIVL output: #{civl_output}".blue
                            if civl_output.include? "Time out."
                              civl_timeouts += 1
                            end
                              if svcomp_error_found(civl_output)
                                underapprox_result = error_found
                                    summary_regex = 'Violation \d encountered at depth .*?(?=\R\R|\z)'
                                    violation_summaries = civl_output.scan(/#{summary_regex}/m).flatten

                                  violation_summaries.each do |violation|
                                    if is_svcomp_error(violation)
                                      trace_id = extract_trace_id(violation)
                                      svcomp_error_summary = violation
                                      break
                                    end
                                  end

                                  unless svcomp_error_summary.include? "certainty: PROVEABLE"
                                    assert { puts "CIVL cannot prove error with certainty: need to specialize."; false }
                                  end

                              elsif no_relevant_error_found(civl_output)
                                underapprox_result = safe
                                spurious_error_count += 1
                              elsif timed_out(civl_output)
                                underapprox_result = unknown # For timeouts
                              else
                                assert { puts "CIVL messed up somehow: investigate. Aborting.".red; false }
                              end

                              message = "  CIVL result: #{result_string(underapprox_result)}"
                              log.write message

                            if civl_debug
                                civl_out_file = "#{program_dir}/civl.iter#{iteration}.out"
                                under_instr_iter_program = "#{under_instr_program}.#{iteration}.c"
                                unless File.exist? under_instr_iter_program
                                  `cp #{under_instr_program} #{under_instr_iter_program}` 
                                end
                                directive_iter_file = "#{guidance_file}.#{iteration}"
                                `cp #{guidance_file} #{directive_iter_file}`
                                  f = directive_iter_file
                                  # The regular expression just looks for any line containing a '.'
                                  # and replaces this with the named of the debugging file name
                                  debug_c_file = under_instr_iter_program.split('/')[-1] # Don't want absolute path
                                  File.write(f, File.read(f).gsub(/.*\..*/, debug_c_file))


                                # Grab everything up to the directive option
                                civl_verify_cmd_prefix = civl_verify_cmd[/(.*)\s-direct=/,1]
                                civl_debug_verify_cmd = "#{civl_verify_cmd_prefix} "\
                                                        "-direct=#{directive_iter_file} "\
                                                        "#{under_instr_iter_program}"

                                header = "Running:\n\n"\
                                         "  #{civl_debug_verify_cmd}\n\n\n"

                                File.open(civl_out_file, 'w') {|f| f.write(header+civl_output)}

                            end

                          if underapprox_result == error_found
                                slice_cmd = "#{civl_jar} replay -id=#{trace_id} -sliceAnalysis #{under_instr_program}"
                                slice_output = `#{slice_cmd}`
                                if civl_debug
                                    message = "\n\n=============== REPLAY =================\n\n"
                                    File.open(civl_out_file, 'a') {|f| f.write(message+slice_output)}

                                end
                                  new_input_counts = parse_input_frequency(slice_output)
                                  input_counts = update_input_counts(input_counts, new_input_counts)

                                  new_input_types = parse_input_types(slice_output)
                                  input_types = update_input_types(input_types, new_input_types)

                                  new_symvars_to_lines = parse_read_calls(slice_output)
                                  symvars_to_lines = update_symvars_to_lines(symvars_to_lines, new_symvars_to_lines)

                                  new_lines_to_vars = make_line_to_var_map(slice_output)
                                  $lines_to_vars = update_lines_to_vars($lines_to_vars, new_lines_to_vars)

                                  sliced_away_count = get_number_sliced_away(slice_output)
                                  conjuncts_sliced += sliced_away_count

                                  sliced_pc = parse_assumptions(slice_output, symvars_to_lines)
                                  if sliced_pc.empty?
                                    always_fails = true
                                    break
                                  end
                                  $last_blocking_pc = sliced_pc
                                #  lines = parse_instrument_lines(slice_output)


                                disjoint_covering[sliced_pc] = [sliced_pc]
                                  message = "\n  Adding new entry to disjoint covering:\n"\
                                              "    #{sliced_pc}\n"
                                  log.write message



                          elsif underapprox_result == safe
                              trying_to_block_spurious_errors = true
                              if trying_to_block_spurious_errors
                                    civl_rerun_verify_cmd = "#{civl_jar} verify "\
                                                            "-svcomp16 "\
                                                            "-direct=#{guidance_file} "\
                                                            "-logTraceOnBacktrack "\
                                                            "#{under_instr_program}"
                                    puts "Rerunning CIVL with cmd: #{civl_rerun_verify_cmd}".yellow
                                    civl_rerun_output = `#{civl_rerun_verify_cmd}`

                                    replay_spurious_cmd = "#{civl_jar} replay -logTraceOnBacktrack #{under_instr_program}"
                                    civl_spurious_replay_output = `#{replay_spurious_cmd}`
                                    puts civl_spurious_replay_output.red

                                    new_input_counts = parse_input_frequency(civl_spurious_replay_output)
                                    input_counts = update_input_counts(input_counts, new_input_counts)

                                    new_input_types = parse_input_types(civl_spurious_replay_output)
                                    input_types = update_input_types(input_types, new_input_types)

                                    new_symvars_to_lines = parse_read_calls(civl_spurious_replay_output)
                                    symvars_to_lines = update_symvars_to_lines(symvars_to_lines, new_symvars_to_lines)

                                    new_lines_to_vars = make_line_to_var_map(civl_spurious_replay_output)
                                    $lines_to_vars = update_lines_to_vars($lines_to_vars, new_lines_to_vars)

                                  spurious_pc = parse_assumptions(civl_spurious_replay_output, symvars_to_lines)

                                log.write("\n\n  Spurious error PC:\n   #{spurious_pc}\n")
                                unless spurious_pc.empty?
                                  spurious_errors << spurious_pc
                                end
                                if civl_debug
                                    civl_rerun_verify_debug_cmd = "#{civl_jar} verify "\
                                                                  "-svcomp16 "\
                                                                  "-direct=#{directive_iter_file} "\
                                                                  "-logTraceOnBacktrack "\
                                                                  "#{under_instr_iter_program}"
                                    header = "\n\n  ** Rerunning to find spurious error:\n\n  #{civl_rerun_verify_debug_cmd}\n\n"
                                    File.open(civl_out_file, 'a') {|f| f.write(header+civl_rerun_output)}

                                end
                              else
                                  unless just_generalized
                                    overapprox_result = unknown
                                    underapprox_result = unknown
                                  end
                                    message = "\n  ==== Disjoint Covering after iteration #{iteration} ====\n\n"\
                                                "#{make_bracketed_pairs(disjoint_covering)}"
                                    log.write message

                                  iteration += 1
                                  log.write_banner(iteration)
                                    # Don't shuffle while debugging trex
                                    # TODO: make this a shift, so this analysis is deterministic
                                    #configurations.shuffle!


                                next
                              end

                          end

                        generalization_stopwatch.stop

                        possible_failure_stopwatch.stop
                        definite_failure_stopwatch.start
                                problematic_line_guidance_file = "#{guidance_file}.problematic_line"
                                `cp #{guidance_file} #{problematic_line_guidance_file}`
                                rewrite_line_with_string(problematic_line_guidance_file, 1, instr_program_with_problematic_line.split('/').last)

                              bound = 42
                              civl_verify_cmd = "#{civl_jar} verify "\
                                                "-svcomp16 "\
                                                "-timeout=#{$options[:civl_timeout]} "\
                                                "-errorBound=#{bound} "\
                                                "-direct=#{problematic_line_guidance_file} "\
                                                "#{instr_program_with_problematic_line}"


                            civl_output = `#{civl_verify_cmd}`

                            if civl_output.include? "Time out."
                              civl_timeouts += 1
                            end
                              if svcomp_error_found(civl_output)
                                underapprox_result = error_found
                                    summary_regex = 'Violation \d encountered at depth .*?(?=\R\R|\z)'
                                    violation_summaries = civl_output.scan(/#{summary_regex}/m).flatten

                                  violation_summaries.each do |violation|
                                    if is_svcomp_error(violation)
                                      trace_id = extract_trace_id(violation)
                                      svcomp_error_summary = violation
                                      break
                                    end
                                  end

                                  unless svcomp_error_summary.include? "certainty: PROVEABLE"
                                    assert { puts "CIVL cannot prove error with certainty: need to specialize."; false }
                                  end

                              elsif no_relevant_error_found(civl_output)
                                underapprox_result = safe
                                spurious_error_count += 1
                              elsif timed_out(civl_output)
                                underapprox_result = unknown # For timeouts
                              else
                                assert { puts "CIVL messed up somehow: investigate. Aborting.".red; false }
                              end

                              message = "  CIVL result: #{result_string(underapprox_result)}"
                              log.write message


                          if underapprox_result == error_found
                              problematic_statement_blocking_clause = $last_blocking_pc.dup
                                slice_cmd = "#{civl_jar} replay -id=#{trace_id} -sliceAnalysis #{instr_program_with_problematic_line}"
                                slice_output = `#{slice_cmd}`
                                if civl_debug
                                    message = "\n\n=============== REPLAY =================\n\n"
                                    File.open(civl_out_file, 'a') {|f| f.write(message+slice_output)}

                                end
                                  symvars_to_lines = parse_read_calls(slice_output)
                                  problematic_sliced_pc = parse_assumptions(slice_output, symvars_to_lines)


                              disjoint_covering[problematic_statement_blocking_clause] = problematic_sliced_pc
                              message = "  Disjoint element: #{problematic_statement_blocking_clause} now has an image of:\n    #{problematic_sliced_pc}"
                              log.write message

                          elsif underapprox_result == safe
                              trying_to_block_spurious_errors = true
                              if trying_to_block_spurious_errors
                                    civl_rerun_verify_cmd = "#{civl_jar} verify "\
                                                            "-svcomp16 "\
                                                            "-direct=#{guidance_file} "\
                                                            "-logTraceOnBacktrack "\
                                                            "#{under_instr_program}"
                                    puts "Rerunning CIVL with cmd: #{civl_rerun_verify_cmd}".yellow
                                    civl_rerun_output = `#{civl_rerun_verify_cmd}`

                                    replay_spurious_cmd = "#{civl_jar} replay -logTraceOnBacktrack #{under_instr_program}"
                                    civl_spurious_replay_output = `#{replay_spurious_cmd}`
                                    puts civl_spurious_replay_output.red

                                    new_input_counts = parse_input_frequency(civl_spurious_replay_output)
                                    input_counts = update_input_counts(input_counts, new_input_counts)

                                    new_input_types = parse_input_types(civl_spurious_replay_output)
                                    input_types = update_input_types(input_types, new_input_types)

                                    new_symvars_to_lines = parse_read_calls(civl_spurious_replay_output)
                                    symvars_to_lines = update_symvars_to_lines(symvars_to_lines, new_symvars_to_lines)

                                    new_lines_to_vars = make_line_to_var_map(civl_spurious_replay_output)
                                    $lines_to_vars = update_lines_to_vars($lines_to_vars, new_lines_to_vars)

                                  spurious_pc = parse_assumptions(civl_spurious_replay_output, symvars_to_lines)

                                log.write("\n\n  Spurious error PC:\n   #{spurious_pc}\n")
                                unless spurious_pc.empty?
                                  spurious_errors << spurious_pc
                                end
                                if civl_debug
                                    civl_rerun_verify_debug_cmd = "#{civl_jar} verify "\
                                                                  "-svcomp16 "\
                                                                  "-direct=#{directive_iter_file} "\
                                                                  "-logTraceOnBacktrack "\
                                                                  "#{under_instr_iter_program}"
                                    header = "\n\n  ** Rerunning to find spurious error:\n\n  #{civl_rerun_verify_debug_cmd}\n\n"
                                    File.open(civl_out_file, 'a') {|f| f.write(header+civl_rerun_output)}

                                end
                              else
                                  unless just_generalized
                                    overapprox_result = unknown
                                    underapprox_result = unknown
                                  end
                                    message = "\n  ==== Disjoint Covering after iteration #{iteration} ====\n\n"\
                                                "#{make_bracketed_pairs(disjoint_covering)}"
                                    log.write message

                                  iteration += 1
                                  log.write_banner(iteration)
                                    # Don't shuffle while debugging trex
                                    # TODO: make this a shift, so this analysis is deterministic
                                    #configurations.shuffle!


                                next
                              end

                          end

                        definite_failure_stopwatch.stop
                        possible_failure_stopwatch.start

                          unless just_generalized
                            upper_characterization = disjoint_covering.keys
                            clauses_to_block = upper_characterization + spurious_errors
                            over_instr_program = instrument(over_instr_program, clauses_to_block, symvars_to_lines, input_counts, input_types)
                            under_instr_program = instrument(under_instr_program, clauses_to_block, symvars_to_lines, input_counts, input_types)
                          end

                          unless just_generalized
                            overapprox_result = unknown
                            underapprox_result = unknown
                          end
                            message = "\n  ==== Disjoint Covering after iteration #{iteration} ====\n\n"\
                                        "#{make_bracketed_pairs(disjoint_covering)}"
                            log.write message

                          iteration += 1
                          log.write_banner(iteration)
                            # Don't shuffle while debugging trex
                            # TODO: make this a shift, so this analysis is deterministic
                            #configurations.shuffle!


                        goto_beginning_of_analysis_loop = true
                        next
                      end

                        if ua_output.include? "SyntaxErrorResult [Line:"
                          message = "UltimateAutomizer reported a syntax error. Aborting."
                          log.write message
                          assert { puts message.red; false }
                        end

                        if ua_output.include? "Result:\nTRUE"
                          overapprox_result = safe
                        elsif ua_output.include? "Result:\nFALSE"
                            witness = "#{program_dir}/witness.graphml"
                            is_cpa = false
                            abstract_witness_object = get_abstract_witness(witness, is_cpa)
                            abstract_witness = get_canonical_string(abstract_witness_object)

                          if witness_cache.include? abstract_witness
                            overapprox_result = cached 
                          else
                            overapprox_result = error_found 
                            witness_cache << abstract_witness
                              guidance_file = make_guidance_file(abstract_witness_object, program_dir, under_instr_program)

                          end
                        else
                          overapprox_result = unknown
                        end


                  end
                elsif conf.first == "CPA"
                  if $options[:only_ua]
                    next
                  else
                      c = conf[1] # grab configuration flag for CPA
                      if ($options[:single_cpa] && (c != "sv-comp17"))
                        next
                      else
                          cpa_cmd = "timeout #{$options[:cpa_timeout]} "\
                                    "#{cpa_script} -#{c} "\
                                    "-timelimit 900 "\
                                    "-spec #{cpa_specification} "\
                                    "-setprop output.path=\"#{cpa_output_dir}\" "\
                                    "#{over_instr_program}"
                          cpa_output, cpa_stderr, status = Open3.capture3(cpa_cmd)
                          if cpa_debug
                              config_out_file = "#{program_dir}/#{c}.iter#{iteration}.out"
                              over_instr_iter_program = "#{over_instr_program}.#{iteration}.c"
                              `cp #{over_instr_program} #{over_instr_iter_program}` 

                              cpa_cmd_prefix = cpa_cmd[/(.*)\s/,1] # Remove program name from cmd
                              header = "Running:\n\n"\
                                       "  #{cpa_cmd_prefix} #{over_instr_iter_program}\n\n\n"

                              File.open(config_out_file, 'w') {|f| f.write(header+cpa_output)}

                          end
                          if cpa_stderr.include? "Warning: Analysis interrupted (The CPU-time limit"
                            cpa_timeouts += 1
                          end

                          if cpa_output.include? "Verification result: TRUE."
                            overapprox_result = safe
                          elsif cpa_output.include? "Verification result: FALSE."
                              if c.include? "sv-comp17"
                                witness = "#{cpa_output_dir}/violation-witness.graphml"
                              else
                                witness = "#{cpa_output_dir}/witness.graphml"
                                witness_gz = witness+'.gz'
                                `rm #{witness}` if File.exist? witness # from a previous run
                                unless File.exist? witness_gz
                                  assert { puts 'Zipped witness file does not exist in #{cpa_output_dir}. Aborting.'; false }
                                end
                                `gunzip #{witness_gz}`
                              end
                              is_cpa = true
                              abstract_witness_object = get_abstract_witness(witness, is_cpa)
                              abstract_witness = get_canonical_string(abstract_witness_object)

                            if witness_cache.include? abstract_witness
                              overapprox_result = cached 
                            else
                              overapprox_result = error_found 
                              witness_cache << abstract_witness
                                guidance_file = make_guidance_file(abstract_witness_object, program_dir, under_instr_program)

                            end
                          else
                            overapprox_result = unknown
                          end

                      end

                  end
                else
                  assert { puts "Unsupported overapproximation tool: #{conf.first}"; false }
                end
                  if conf.first == "UltimateAutomizer"
                    message = "  UAutomizer result: #{result_string(overapprox_result)}"
                  else
                    message = "  #{c} result: #{result_string(overapprox_result)}"
                  end
                  log.write message

                  unless (overapprox_result == cached || overapprox_result == unknown)
                    break
                  end

                break if goto_beginning_of_analysis_loop
              end
              if goto_beginning_of_analysis_loop
                possible_failure_stopwatch.stop
                next
              end
                message = "\n  -------------\n"\
                            "  Witness cache: #{witness_cache}\n"\
                            "  -------------\n"
                log.write message


            possible_failure_stopwatch.stop
          end

        if overapprox_result == safe
          break
        elsif overapprox_result == error_found
              bound = 42
              civl_verify_cmd = "#{civl_jar} verify "\
                                "-svcomp16 "\
                                "-timeout=#{$options[:civl_timeout]} "\
                                "-errorBound=#{bound} "\
                                "-direct=#{guidance_file} "\
                                "#{under_instr_program}"


            definite_failure_stopwatch.start
            civl_output = `#{civl_verify_cmd}`
            definite_failure_stopwatch.stop

            #puts "CIVL output: #{civl_output}".blue
            if civl_output.include? "Time out."
              civl_timeouts += 1
            end
              if svcomp_error_found(civl_output)
                underapprox_result = error_found
                    summary_regex = 'Violation \d encountered at depth .*?(?=\R\R|\z)'
                    violation_summaries = civl_output.scan(/#{summary_regex}/m).flatten

                  violation_summaries.each do |violation|
                    if is_svcomp_error(violation)
                      trace_id = extract_trace_id(violation)
                      svcomp_error_summary = violation
                      break
                    end
                  end

                  unless svcomp_error_summary.include? "certainty: PROVEABLE"
                    assert { puts "CIVL cannot prove error with certainty: need to specialize."; false }
                  end

              elsif no_relevant_error_found(civl_output)
                underapprox_result = safe
                spurious_error_count += 1
              elsif timed_out(civl_output)
                underapprox_result = unknown # For timeouts
              else
                assert { puts "CIVL messed up somehow: investigate. Aborting.".red; false }
              end

              message = "  CIVL result: #{result_string(underapprox_result)}"
              log.write message

            if civl_debug
                civl_out_file = "#{program_dir}/civl.iter#{iteration}.out"
                under_instr_iter_program = "#{under_instr_program}.#{iteration}.c"
                unless File.exist? under_instr_iter_program
                  `cp #{under_instr_program} #{under_instr_iter_program}` 
                end
                directive_iter_file = "#{guidance_file}.#{iteration}"
                `cp #{guidance_file} #{directive_iter_file}`
                  f = directive_iter_file
                  # The regular expression just looks for any line containing a '.'
                  # and replaces this with the named of the debugging file name
                  debug_c_file = under_instr_iter_program.split('/')[-1] # Don't want absolute path
                  File.write(f, File.read(f).gsub(/.*\..*/, debug_c_file))


                # Grab everything up to the directive option
                civl_verify_cmd_prefix = civl_verify_cmd[/(.*)\s-direct=/,1]
                civl_debug_verify_cmd = "#{civl_verify_cmd_prefix} "\
                                        "-direct=#{directive_iter_file} "\
                                        "#{under_instr_iter_program}"

                header = "Running:\n\n"\
                         "  #{civl_debug_verify_cmd}\n\n\n"

                File.open(civl_out_file, 'w') {|f| f.write(header+civl_output)}

            end

          if underapprox_result == error_found
            slice_stopwatch.start
                slice_cmd = "#{civl_jar} replay -id=#{trace_id} -sliceAnalysis #{under_instr_program}"
                slice_output = `#{slice_cmd}`
                if civl_debug
                    message = "\n\n=============== REPLAY =================\n\n"
                    File.open(civl_out_file, 'a') {|f| f.write(message+slice_output)}

                end
                  new_input_counts = parse_input_frequency(slice_output)
                  input_counts = update_input_counts(input_counts, new_input_counts)

                  new_input_types = parse_input_types(slice_output)
                  input_types = update_input_types(input_types, new_input_types)

                  new_symvars_to_lines = parse_read_calls(slice_output)
                  symvars_to_lines = update_symvars_to_lines(symvars_to_lines, new_symvars_to_lines)

                  new_lines_to_vars = make_line_to_var_map(slice_output)
                  $lines_to_vars = update_lines_to_vars($lines_to_vars, new_lines_to_vars)

                  sliced_away_count = get_number_sliced_away(slice_output)
                  conjuncts_sliced += sliced_away_count

                  sliced_pc = parse_assumptions(slice_output, symvars_to_lines)
                  if sliced_pc.empty?
                    always_fails = true
                    break
                  end
                  $last_blocking_pc = sliced_pc
                #  lines = parse_instrument_lines(slice_output)


                disjoint_covering[sliced_pc] = [sliced_pc]
                  message = "\n  Adding new entry to disjoint covering:\n"\
                              "    #{sliced_pc}\n"
                  log.write message



            slice_stopwatch.stop
          elsif underapprox_result == safe
              trying_to_block_spurious_errors = true
              if trying_to_block_spurious_errors
                    civl_rerun_verify_cmd = "#{civl_jar} verify "\
                                            "-svcomp16 "\
                                            "-direct=#{guidance_file} "\
                                            "-logTraceOnBacktrack "\
                                            "#{under_instr_program}"
                    puts "Rerunning CIVL with cmd: #{civl_rerun_verify_cmd}".yellow
                    civl_rerun_output = `#{civl_rerun_verify_cmd}`

                    replay_spurious_cmd = "#{civl_jar} replay -logTraceOnBacktrack #{under_instr_program}"
                    civl_spurious_replay_output = `#{replay_spurious_cmd}`
                    puts civl_spurious_replay_output.red

                    new_input_counts = parse_input_frequency(civl_spurious_replay_output)
                    input_counts = update_input_counts(input_counts, new_input_counts)

                    new_input_types = parse_input_types(civl_spurious_replay_output)
                    input_types = update_input_types(input_types, new_input_types)

                    new_symvars_to_lines = parse_read_calls(civl_spurious_replay_output)
                    symvars_to_lines = update_symvars_to_lines(symvars_to_lines, new_symvars_to_lines)

                    new_lines_to_vars = make_line_to_var_map(civl_spurious_replay_output)
                    $lines_to_vars = update_lines_to_vars($lines_to_vars, new_lines_to_vars)

                  spurious_pc = parse_assumptions(civl_spurious_replay_output, symvars_to_lines)

                log.write("\n\n  Spurious error PC:\n   #{spurious_pc}\n")
                unless spurious_pc.empty?
                  spurious_errors << spurious_pc
                end
                if civl_debug
                    civl_rerun_verify_debug_cmd = "#{civl_jar} verify "\
                                                  "-svcomp16 "\
                                                  "-direct=#{directive_iter_file} "\
                                                  "-logTraceOnBacktrack "\
                                                  "#{under_instr_iter_program}"
                    header = "\n\n  ** Rerunning to find spurious error:\n\n  #{civl_rerun_verify_debug_cmd}\n\n"
                    File.open(civl_out_file, 'a') {|f| f.write(header+civl_rerun_output)}

                end
              else
                  unless just_generalized
                    overapprox_result = unknown
                    underapprox_result = unknown
                  end
                    message = "\n  ==== Disjoint Covering after iteration #{iteration} ====\n\n"\
                                "#{make_bracketed_pairs(disjoint_covering)}"
                    log.write message

                  iteration += 1
                  log.write_banner(iteration)
                    # Don't shuffle while debugging trex
                    # TODO: make this a shift, so this analysis is deterministic
                    #configurations.shuffle!


                next
              end

          end
        else
            if iteration == 1
              message = "No overapproximators could find an error on the first iteration. Aborting."
              log.write message; puts message; abort
            end
            generalization_stopwatch.start
              generalization_count += 1
              generalized_clause = top_of_lattice
                disjoint_covering.delete($last_blocking_pc)

                upper_approximation = disjoint_covering.keys
                base = find_maximally_intersecting_set(upper_approximation, $last_blocking_pc)
                candidates = []

                if $last_blocking_pc
              #    candidate_clause = $last_blocking_pc - base
              #    candidates = get_candidate_powerset(candidate_clause)
                  clause_size = $last_blocking_pc.size
                  if clause_size > 3
                      candidates.concat $last_blocking_pc.combination(1).to_a[0..2]
                      
                  elsif clause_size > 2
                      candidates.concat $last_blocking_pc.combination(2).to_a
                      candidates.concat $last_blocking_pc.combination(1).to_a

                  elsif clause_size > 1
                      candidates.concat $last_blocking_pc.combination(1).to_a

                  else
                        analysis_moved_to_top = true
                        log.write "\n\n  ########## BLOCKING TOP ##########\n"
                        puts "Blocking TOP".green
                        overapprox_result = safe
                        just_generalized = true

                  end
                else
                      analysis_moved_to_top = true
                      log.write "\n\n  ########## BLOCKING TOP ##########\n"
                      puts "Blocking TOP".green
                      overapprox_result = safe
                      just_generalized = true

                end


              message = "The candidate clauses in this generalization attempt are: #{candidates}"

              candidates.each do |clause_set|
                iffy_generalization = $last_blocking_pc - clause_set
                  if iffy_generalization.flatten.empty?
                    iffy_generalization = top_of_lattice
                        analysis_moved_to_top = true
                        log.write "\n\n  ########## BLOCKING TOP ##########\n"
                        puts "Blocking TOP".green
                        overapprox_result = safe
                        just_generalized = true

                  end

                unless analysis_moved_to_top
                    upper_characterization = disjoint_covering.keys
                    blocking_clauses = upper_characterization + [iffy_generalization] + spurious_errors
                    puts "Blocking clauses in generalization: #{blocking_clauses}".red
                  # instrumented_program = instrument(instrumented_program, blocking_clauses, symvars_to_lines, input_counts, input_types)
                    over_instr_program = instrument(over_instr_program, blocking_clauses, symvars_to_lines, input_counts, input_types)
                    under_instr_program = instrument(under_instr_program, blocking_clauses, symvars_to_lines, input_counts, input_types)

                    message = "\n  ** Attempting to generalize **\n\n"\
                                "    Dropping:\n"\
                                "      #{clause_set}\n"\
                                "    Tentatively blocking:\n"\
                                "      #{iffy_generalization}\n"
                    log.write message

                  
                    goto_beginning_of_analysis_loop = false

                    configurations.each do |conf|
                      if conf.first == "UltimateAutomizer"
                        if $options[:only_cpa]
                          next
                        else
                            ua_cmd = "#{ua_script} "\
                                     "--full-output "\
                                     "--spec #{ua_specification} "\
                                     "--architecture 32bit "\
                                     "--file #{over_instr_program} "\
                                     "--witness-dir #{program_dir}"
                            ua_output = `timeout #{$options[:ua_timeout]} #{ua_cmd}`
                            #puts ua_output.yellow
                            if $?.exitstatus == TIMEOUT_STATUS
                              ua_timeouts += 1
                            end
                            File.open("#{program_dir}/Ultimate.log.#{iteration}","w") {|f| f.write(ua_output)}
                            
                            unsupported_operation_message = "- UnsupportedSyntaxResult [Line: "
                            if ua_output.include? unsupported_operation_message
                                instr_program_with_problematic_line = "#{under_instr_program}.problematic.#{iteration}.c"
                                `cp #{under_instr_program} #{instr_program_with_problematic_line}`


                              generalization_stopwatch.start
                                  problem_line = ua_output.scan(/UnsupportedSyntaxResult \[Line: (\d+)\]/).flatten.first.to_i
                                  message = "  UA found a problem line at: #{problem_line}; prepending error line to instrumented file."
                                  log.write message
                                  prepend_error_at_line(over_instr_program, problem_line)
                                  prepend_error_at_line(under_instr_program, problem_line)
                                  generalization_count += 1
                                    ua_output = `timeout #{$options[:ua_timeout]} #{ua_cmd}`
                                    if $?.exitstatus == TIMEOUT_STATUS
                                      ua_timeouts += 1
                                    end
                                    File.open("#{program_dir}/Ultimate.log.#{iteration}","w") {|f| f.write(ua_output)}  
                                      if ua_output.include? "SyntaxErrorResult [Line:"
                                        message = "UltimateAutomizer reported a syntax error. Aborting."
                                        log.write message
                                        assert { puts message.red; false }
                                      end

                                      if ua_output.include? "Result:\nTRUE"
                                        overapprox_result = safe
                                      elsif ua_output.include? "Result:\nFALSE"
                                          witness = "#{program_dir}/witness.graphml"
                                          is_cpa = false
                                          abstract_witness_object = get_abstract_witness(witness, is_cpa)
                                          abstract_witness = get_canonical_string(abstract_witness_object)

                                        if witness_cache.include? abstract_witness
                                          overapprox_result = cached 
                                        else
                                          overapprox_result = error_found 
                                          witness_cache << abstract_witness
                                            guidance_file = make_guidance_file(abstract_witness_object, program_dir, under_instr_program)

                                        end
                                      else
                                        overapprox_result = unknown
                                      end

                                      if conf.first == "UltimateAutomizer"
                                        message = "  UAutomizer result: #{result_string(overapprox_result)}"
                                      else
                                        message = "  #{c} result: #{result_string(overapprox_result)}"
                                      end
                                      log.write message



                              # Do I classify this chunk as a definite failure? It's a definite overapproximation...
                                    bound = 42
                                    civl_verify_cmd = "#{civl_jar} verify "\
                                                      "-svcomp16 "\
                                                      "-timeout=#{$options[:civl_timeout]} "\
                                                      "-errorBound=#{bound} "\
                                                      "-direct=#{guidance_file} "\
                                                      "#{under_instr_program}"


                                  definite_failure_stopwatch.start
                                  civl_output = `#{civl_verify_cmd}`
                                  definite_failure_stopwatch.stop

                                  #puts "CIVL output: #{civl_output}".blue
                                  if civl_output.include? "Time out."
                                    civl_timeouts += 1
                                  end
                                    if svcomp_error_found(civl_output)
                                      underapprox_result = error_found
                                          summary_regex = 'Violation \d encountered at depth .*?(?=\R\R|\z)'
                                          violation_summaries = civl_output.scan(/#{summary_regex}/m).flatten

                                        violation_summaries.each do |violation|
                                          if is_svcomp_error(violation)
                                            trace_id = extract_trace_id(violation)
                                            svcomp_error_summary = violation
                                            break
                                          end
                                        end

                                        unless svcomp_error_summary.include? "certainty: PROVEABLE"
                                          assert { puts "CIVL cannot prove error with certainty: need to specialize."; false }
                                        end

                                    elsif no_relevant_error_found(civl_output)
                                      underapprox_result = safe
                                      spurious_error_count += 1
                                    elsif timed_out(civl_output)
                                      underapprox_result = unknown # For timeouts
                                    else
                                      assert { puts "CIVL messed up somehow: investigate. Aborting.".red; false }
                                    end

                                    message = "  CIVL result: #{result_string(underapprox_result)}"
                                    log.write message

                                  if civl_debug
                                      civl_out_file = "#{program_dir}/civl.iter#{iteration}.out"
                                      under_instr_iter_program = "#{under_instr_program}.#{iteration}.c"
                                      unless File.exist? under_instr_iter_program
                                        `cp #{under_instr_program} #{under_instr_iter_program}` 
                                      end
                                      directive_iter_file = "#{guidance_file}.#{iteration}"
                                      `cp #{guidance_file} #{directive_iter_file}`
                                        f = directive_iter_file
                                        # The regular expression just looks for any line containing a '.'
                                        # and replaces this with the named of the debugging file name
                                        debug_c_file = under_instr_iter_program.split('/')[-1] # Don't want absolute path
                                        File.write(f, File.read(f).gsub(/.*\..*/, debug_c_file))


                                      # Grab everything up to the directive option
                                      civl_verify_cmd_prefix = civl_verify_cmd[/(.*)\s-direct=/,1]
                                      civl_debug_verify_cmd = "#{civl_verify_cmd_prefix} "\
                                                              "-direct=#{directive_iter_file} "\
                                                              "#{under_instr_iter_program}"

                                      header = "Running:\n\n"\
                                               "  #{civl_debug_verify_cmd}\n\n\n"

                                      File.open(civl_out_file, 'w') {|f| f.write(header+civl_output)}

                                  end

                                if underapprox_result == error_found
                                      slice_cmd = "#{civl_jar} replay -id=#{trace_id} -sliceAnalysis #{under_instr_program}"
                                      slice_output = `#{slice_cmd}`
                                      if civl_debug
                                          message = "\n\n=============== REPLAY =================\n\n"
                                          File.open(civl_out_file, 'a') {|f| f.write(message+slice_output)}

                                      end
                                        new_input_counts = parse_input_frequency(slice_output)
                                        input_counts = update_input_counts(input_counts, new_input_counts)

                                        new_input_types = parse_input_types(slice_output)
                                        input_types = update_input_types(input_types, new_input_types)

                                        new_symvars_to_lines = parse_read_calls(slice_output)
                                        symvars_to_lines = update_symvars_to_lines(symvars_to_lines, new_symvars_to_lines)

                                        new_lines_to_vars = make_line_to_var_map(slice_output)
                                        $lines_to_vars = update_lines_to_vars($lines_to_vars, new_lines_to_vars)

                                        sliced_away_count = get_number_sliced_away(slice_output)
                                        conjuncts_sliced += sliced_away_count

                                        sliced_pc = parse_assumptions(slice_output, symvars_to_lines)
                                        if sliced_pc.empty?
                                          always_fails = true
                                          break
                                        end
                                        $last_blocking_pc = sliced_pc
                                      #  lines = parse_instrument_lines(slice_output)


                                      disjoint_covering[sliced_pc] = [sliced_pc]
                                        message = "\n  Adding new entry to disjoint covering:\n"\
                                                    "    #{sliced_pc}\n"
                                        log.write message



                                elsif underapprox_result == safe
                                    trying_to_block_spurious_errors = true
                                    if trying_to_block_spurious_errors
                                          civl_rerun_verify_cmd = "#{civl_jar} verify "\
                                                                  "-svcomp16 "\
                                                                  "-direct=#{guidance_file} "\
                                                                  "-logTraceOnBacktrack "\
                                                                  "#{under_instr_program}"
                                          puts "Rerunning CIVL with cmd: #{civl_rerun_verify_cmd}".yellow
                                          civl_rerun_output = `#{civl_rerun_verify_cmd}`

                                          replay_spurious_cmd = "#{civl_jar} replay -logTraceOnBacktrack #{under_instr_program}"
                                          civl_spurious_replay_output = `#{replay_spurious_cmd}`
                                          puts civl_spurious_replay_output.red

                                          new_input_counts = parse_input_frequency(civl_spurious_replay_output)
                                          input_counts = update_input_counts(input_counts, new_input_counts)

                                          new_input_types = parse_input_types(civl_spurious_replay_output)
                                          input_types = update_input_types(input_types, new_input_types)

                                          new_symvars_to_lines = parse_read_calls(civl_spurious_replay_output)
                                          symvars_to_lines = update_symvars_to_lines(symvars_to_lines, new_symvars_to_lines)

                                          new_lines_to_vars = make_line_to_var_map(civl_spurious_replay_output)
                                          $lines_to_vars = update_lines_to_vars($lines_to_vars, new_lines_to_vars)

                                        spurious_pc = parse_assumptions(civl_spurious_replay_output, symvars_to_lines)

                                      log.write("\n\n  Spurious error PC:\n   #{spurious_pc}\n")
                                      unless spurious_pc.empty?
                                        spurious_errors << spurious_pc
                                      end
                                      if civl_debug
                                          civl_rerun_verify_debug_cmd = "#{civl_jar} verify "\
                                                                        "-svcomp16 "\
                                                                        "-direct=#{directive_iter_file} "\
                                                                        "-logTraceOnBacktrack "\
                                                                        "#{under_instr_iter_program}"
                                          header = "\n\n  ** Rerunning to find spurious error:\n\n  #{civl_rerun_verify_debug_cmd}\n\n"
                                          File.open(civl_out_file, 'a') {|f| f.write(header+civl_rerun_output)}

                                      end
                                    else
                                        unless just_generalized
                                          overapprox_result = unknown
                                          underapprox_result = unknown
                                        end
                                          message = "\n  ==== Disjoint Covering after iteration #{iteration} ====\n\n"\
                                                      "#{make_bracketed_pairs(disjoint_covering)}"
                                          log.write message

                                        iteration += 1
                                        log.write_banner(iteration)
                                          # Don't shuffle while debugging trex
                                          # TODO: make this a shift, so this analysis is deterministic
                                          #configurations.shuffle!


                                      next
                                    end

                                end

                              generalization_stopwatch.stop

                              possible_failure_stopwatch.stop
                              definite_failure_stopwatch.start
                                      problematic_line_guidance_file = "#{guidance_file}.problematic_line"
                                      `cp #{guidance_file} #{problematic_line_guidance_file}`
                                      rewrite_line_with_string(problematic_line_guidance_file, 1, instr_program_with_problematic_line.split('/').last)

                                    bound = 42
                                    civl_verify_cmd = "#{civl_jar} verify "\
                                                      "-svcomp16 "\
                                                      "-timeout=#{$options[:civl_timeout]} "\
                                                      "-errorBound=#{bound} "\
                                                      "-direct=#{problematic_line_guidance_file} "\
                                                      "#{instr_program_with_problematic_line}"


                                  civl_output = `#{civl_verify_cmd}`

                                  if civl_output.include? "Time out."
                                    civl_timeouts += 1
                                  end
                                    if svcomp_error_found(civl_output)
                                      underapprox_result = error_found
                                          summary_regex = 'Violation \d encountered at depth .*?(?=\R\R|\z)'
                                          violation_summaries = civl_output.scan(/#{summary_regex}/m).flatten

                                        violation_summaries.each do |violation|
                                          if is_svcomp_error(violation)
                                            trace_id = extract_trace_id(violation)
                                            svcomp_error_summary = violation
                                            break
                                          end
                                        end

                                        unless svcomp_error_summary.include? "certainty: PROVEABLE"
                                          assert { puts "CIVL cannot prove error with certainty: need to specialize."; false }
                                        end

                                    elsif no_relevant_error_found(civl_output)
                                      underapprox_result = safe
                                      spurious_error_count += 1
                                    elsif timed_out(civl_output)
                                      underapprox_result = unknown # For timeouts
                                    else
                                      assert { puts "CIVL messed up somehow: investigate. Aborting.".red; false }
                                    end

                                    message = "  CIVL result: #{result_string(underapprox_result)}"
                                    log.write message


                                if underapprox_result == error_found
                                    problematic_statement_blocking_clause = $last_blocking_pc.dup
                                      slice_cmd = "#{civl_jar} replay -id=#{trace_id} -sliceAnalysis #{instr_program_with_problematic_line}"
                                      slice_output = `#{slice_cmd}`
                                      if civl_debug
                                          message = "\n\n=============== REPLAY =================\n\n"
                                          File.open(civl_out_file, 'a') {|f| f.write(message+slice_output)}

                                      end
                                        symvars_to_lines = parse_read_calls(slice_output)
                                        problematic_sliced_pc = parse_assumptions(slice_output, symvars_to_lines)


                                    disjoint_covering[problematic_statement_blocking_clause] = problematic_sliced_pc
                                    message = "  Disjoint element: #{problematic_statement_blocking_clause} now has an image of:\n    #{problematic_sliced_pc}"
                                    log.write message

                                elsif underapprox_result == safe
                                    trying_to_block_spurious_errors = true
                                    if trying_to_block_spurious_errors
                                          civl_rerun_verify_cmd = "#{civl_jar} verify "\
                                                                  "-svcomp16 "\
                                                                  "-direct=#{guidance_file} "\
                                                                  "-logTraceOnBacktrack "\
                                                                  "#{under_instr_program}"
                                          puts "Rerunning CIVL with cmd: #{civl_rerun_verify_cmd}".yellow
                                          civl_rerun_output = `#{civl_rerun_verify_cmd}`

                                          replay_spurious_cmd = "#{civl_jar} replay -logTraceOnBacktrack #{under_instr_program}"
                                          civl_spurious_replay_output = `#{replay_spurious_cmd}`
                                          puts civl_spurious_replay_output.red

                                          new_input_counts = parse_input_frequency(civl_spurious_replay_output)
                                          input_counts = update_input_counts(input_counts, new_input_counts)

                                          new_input_types = parse_input_types(civl_spurious_replay_output)
                                          input_types = update_input_types(input_types, new_input_types)

                                          new_symvars_to_lines = parse_read_calls(civl_spurious_replay_output)
                                          symvars_to_lines = update_symvars_to_lines(symvars_to_lines, new_symvars_to_lines)

                                          new_lines_to_vars = make_line_to_var_map(civl_spurious_replay_output)
                                          $lines_to_vars = update_lines_to_vars($lines_to_vars, new_lines_to_vars)

                                        spurious_pc = parse_assumptions(civl_spurious_replay_output, symvars_to_lines)

                                      log.write("\n\n  Spurious error PC:\n   #{spurious_pc}\n")
                                      unless spurious_pc.empty?
                                        spurious_errors << spurious_pc
                                      end
                                      if civl_debug
                                          civl_rerun_verify_debug_cmd = "#{civl_jar} verify "\
                                                                        "-svcomp16 "\
                                                                        "-direct=#{directive_iter_file} "\
                                                                        "-logTraceOnBacktrack "\
                                                                        "#{under_instr_iter_program}"
                                          header = "\n\n  ** Rerunning to find spurious error:\n\n  #{civl_rerun_verify_debug_cmd}\n\n"
                                          File.open(civl_out_file, 'a') {|f| f.write(header+civl_rerun_output)}

                                      end
                                    else
                                        unless just_generalized
                                          overapprox_result = unknown
                                          underapprox_result = unknown
                                        end
                                          message = "\n  ==== Disjoint Covering after iteration #{iteration} ====\n\n"\
                                                      "#{make_bracketed_pairs(disjoint_covering)}"
                                          log.write message

                                        iteration += 1
                                        log.write_banner(iteration)
                                          # Don't shuffle while debugging trex
                                          # TODO: make this a shift, so this analysis is deterministic
                                          #configurations.shuffle!


                                      next
                                    end

                                end

                              definite_failure_stopwatch.stop
                              possible_failure_stopwatch.start

                                unless just_generalized
                                  upper_characterization = disjoint_covering.keys
                                  clauses_to_block = upper_characterization + spurious_errors
                                  over_instr_program = instrument(over_instr_program, clauses_to_block, symvars_to_lines, input_counts, input_types)
                                  under_instr_program = instrument(under_instr_program, clauses_to_block, symvars_to_lines, input_counts, input_types)
                                end

                                unless just_generalized
                                  overapprox_result = unknown
                                  underapprox_result = unknown
                                end
                                  message = "\n  ==== Disjoint Covering after iteration #{iteration} ====\n\n"\
                                              "#{make_bracketed_pairs(disjoint_covering)}"
                                  log.write message

                                iteration += 1
                                log.write_banner(iteration)
                                  # Don't shuffle while debugging trex
                                  # TODO: make this a shift, so this analysis is deterministic
                                  #configurations.shuffle!


                              goto_beginning_of_analysis_loop = true
                              next
                            end

                              if ua_output.include? "SyntaxErrorResult [Line:"
                                message = "UltimateAutomizer reported a syntax error. Aborting."
                                log.write message
                                assert { puts message.red; false }
                              end

                              if ua_output.include? "Result:\nTRUE"
                                overapprox_result = safe
                              elsif ua_output.include? "Result:\nFALSE"
                                  witness = "#{program_dir}/witness.graphml"
                                  is_cpa = false
                                  abstract_witness_object = get_abstract_witness(witness, is_cpa)
                                  abstract_witness = get_canonical_string(abstract_witness_object)

                                if witness_cache.include? abstract_witness
                                  overapprox_result = cached 
                                else
                                  overapprox_result = error_found 
                                  witness_cache << abstract_witness
                                    guidance_file = make_guidance_file(abstract_witness_object, program_dir, under_instr_program)

                                end
                              else
                                overapprox_result = unknown
                              end


                        end
                      elsif conf.first == "CPA"
                        if $options[:only_ua]
                          next
                        else
                            c = conf[1] # grab configuration flag for CPA
                            if ($options[:single_cpa] && (c != "sv-comp17"))
                              next
                            else
                                cpa_cmd = "timeout #{$options[:cpa_timeout]} "\
                                          "#{cpa_script} -#{c} "\
                                          "-timelimit 900 "\
                                          "-spec #{cpa_specification} "\
                                          "-setprop output.path=\"#{cpa_output_dir}\" "\
                                          "#{over_instr_program}"
                                cpa_output, cpa_stderr, status = Open3.capture3(cpa_cmd)
                                if cpa_debug
                                    config_out_file = "#{program_dir}/#{c}.iter#{iteration}.out"
                                    over_instr_iter_program = "#{over_instr_program}.#{iteration}.c"
                                    `cp #{over_instr_program} #{over_instr_iter_program}` 

                                    cpa_cmd_prefix = cpa_cmd[/(.*)\s/,1] # Remove program name from cmd
                                    header = "Running:\n\n"\
                                             "  #{cpa_cmd_prefix} #{over_instr_iter_program}\n\n\n"

                                    File.open(config_out_file, 'w') {|f| f.write(header+cpa_output)}

                                end
                                if cpa_stderr.include? "Warning: Analysis interrupted (The CPU-time limit"
                                  cpa_timeouts += 1
                                end

                                if cpa_output.include? "Verification result: TRUE."
                                  overapprox_result = safe
                                elsif cpa_output.include? "Verification result: FALSE."
                                    if c.include? "sv-comp17"
                                      witness = "#{cpa_output_dir}/violation-witness.graphml"
                                    else
                                      witness = "#{cpa_output_dir}/witness.graphml"
                                      witness_gz = witness+'.gz'
                                      `rm #{witness}` if File.exist? witness # from a previous run
                                      unless File.exist? witness_gz
                                        assert { puts 'Zipped witness file does not exist in #{cpa_output_dir}. Aborting.'; false }
                                      end
                                      `gunzip #{witness_gz}`
                                    end
                                    is_cpa = true
                                    abstract_witness_object = get_abstract_witness(witness, is_cpa)
                                    abstract_witness = get_canonical_string(abstract_witness_object)

                                  if witness_cache.include? abstract_witness
                                    overapprox_result = cached 
                                  else
                                    overapprox_result = error_found 
                                    witness_cache << abstract_witness
                                      guidance_file = make_guidance_file(abstract_witness_object, program_dir, under_instr_program)

                                  end
                                else
                                  overapprox_result = unknown
                                end

                            end

                        end
                      else
                        assert { puts "Unsupported overapproximation tool: #{conf.first}"; false }
                      end
                        if conf.first == "UltimateAutomizer"
                          message = "  UAutomizer result: #{result_string(overapprox_result)}"
                        else
                          message = "  #{c} result: #{result_string(overapprox_result)}"
                        end
                        log.write message

                        unless (overapprox_result == cached || overapprox_result == unknown)
                          break
                        end

                      break if goto_beginning_of_analysis_loop
                    end
                    if goto_beginning_of_analysis_loop
                      possible_failure_stopwatch.stop
                      next
                    end
                      message = "\n  -------------\n"\
                                  "  Witness cache: #{witness_cache}\n"\
                                  "  -------------\n"
                      log.write message


                end
                if ( overapprox_result == safe || overapprox_result == error_found )
                  generalized_clause = iffy_generalization
                  just_generalized = true
                  break
                end
              end

              # If our generalization scheme didn't work, we punt.
              unless just_generalized
                    analysis_moved_to_top = true
                    log.write "\n\n  ########## BLOCKING TOP ##########\n"
                    puts "Blocking TOP".green
                    overapprox_result = safe
                    just_generalized = true

              end

            generalization_stopwatch.stop
              update_disjoint_covering(disjoint_covering, generalized_clause, log)


        end
          unless just_generalized
            upper_characterization = disjoint_covering.keys
            clauses_to_block = upper_characterization + spurious_errors
            over_instr_program = instrument(over_instr_program, clauses_to_block, symvars_to_lines, input_counts, input_types)
            under_instr_program = instrument(under_instr_program, clauses_to_block, symvars_to_lines, input_counts, input_types)
          end

          unless just_generalized
            overapprox_result = unknown
            underapprox_result = unknown
          end
            message = "\n  ==== Disjoint Covering after iteration #{iteration} ====\n\n"\
                        "#{make_bracketed_pairs(disjoint_covering)}"
            log.write message

          iteration += 1
          log.write_banner(iteration)
            # Don't shuffle while debugging trex
            # TODO: make this a shift, so this analysis is deterministic
            #configurations.shuffle!


      end

      analysis_stopwatch.stop
        partitions_without_gap = 0
        disjoint_covering.each do |upper_bound, lower_bound|
          if lower_bound.size == 1
            if upper_bound == lower_bound.first
              partitions_without_gap += 1
            end
          end
        end


      msg = "\n===============================================\n"\
              "================= CFC RESULTS =================\n"\
              "===============================================\n"\
              "\n"\
              "\n"\
              "  Analysis time: #{analysis_stopwatch.time_spent}\n"\
              "  Possible failure time: #{possible_failure_stopwatch.time_spent}\n"\
              "  Generalization time: #{generalization_stopwatch.time_spent}\n"\
              "  Definite failure time: #{definite_failure_stopwatch.time_spent}\n"\
              "  Slice time: #{slice_stopwatch.time_spent}\n"\
              "\n"\
              "  UA timeouts: #{ua_timeouts} (#{$options[:ua_timeout]} seconds)\n"\
              "  CPA timeouts: #{cpa_timeouts} (#{$options[:cpa_timeout]} seconds)\n"\
              "  CIVL timeouts: #{civl_timeouts} (#{$options[:civl_timeout]} seconds)\n"\
              "\n"\
              "  Iterations: #{iteration}\n"\
              "  Generalizations: #{generalization_count}\n"\
              "  Spurious errors: #{spurious_error_count}\n"\
              "\n"\
              "  Conjuncts sliced: #{conjuncts_sliced}\n"\
              "  Total partitions: #{disjoint_covering.size}\n"\
              "  Partitions without gap: #{partitions_without_gap}\n"\
              "\n"\
              "  Final disjoint covering:\n"\
              "#{make_bracketed_pairs(disjoint_covering)}"
      if analysis_moved_to_top
        msg += "  *** Analysis moved to top ***\n"
      end
      if always_fails
        msg += "  *** All executions lead to an error ***\n"
      end
      msg += "\n\n"

      log.write msg
      puts prettify(msg)


